-- This file contains initial statements for

CREATE TABLE Account (
	id BIGINT NOT NULL AUTO_INCREMENT,
	email VARCHAR(254) NOT NULL UNIQUE,
	name VARCHAR(128),
	year INTEGER,
	gender CHAR(1),
	major VARCHAR(128),
	PRIMARY KEY(id)
);

-- Start ids at 101. We can insert/use fake accounts with id < 101 for testing
ALTER TABLE Account AUTO_INCREMENT = 101;

CREATE TABLE MealPlan (
	id BIGINT NOT NULL AUTO_INCREMENT,
	hostid BIGINT,
	time DATETIME NOT NULL,
	foodmood VARCHAR(32) NOT NULL,
	location VARCHAR(32) NOT NULL,
	description VARCHAR(256),
	PRIMARY KEY(id),
	FOREIGN KEY(hostid) REFERENCES Account(id) ON DELETE CASCADE
);

-- Start ids at 101. We can insert/use fake meal plans with id < 101 for testing
ALTER TABLE MealPlan AUTO_INCREMENT = 101;

CREATE TABLE Attending (
	planid BIGINT NOT NULL,
	attendeeid BIGINT NOT NULL,
	PRIMARY KEY(planid, attendeeid),
	FOREIGN KEY(planid) REFERENCES MealPlan(id) ON DELETE CASCADE,
	FOREIGN KEY(attendeeid) REFERENCES Account(id) ON DELETE CASCADE
);

-- Some account data for us to work with
INSERT INTO Account(id, email, name, year, gender, major) VALUES (1, 'test1@uw.edu', 'test1', 3, 'M', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (2, 'test2@uw.edu', 'test2', 4, 'F', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (3, 'test3@uw.edu', 'test3', 3, 'M', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (4, 'test4@uw.edu', 'test4', 3, 'F', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (5, 'test5@uw.edu', 'test5', 2, 'M', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (6, 'test6@uw.edu', 'test6', 3, 'F', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (7, 'test7@uw.edu', 'test7', 3, 'M', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (8, 'test8@uw.edu', 'test8', 3, 'F', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (9, 'test9@uw.edu', 'test9', 3, 'M', 'CSE');
INSERT INTO Account(id, email, name, year, gender, major) VALUES (10, 'test10@uw.edu', 'test10', 3, 'F', 'CSE');

-- Some meal plan data for us to work with
INSERT INTO MealPlan VALUES (1, 1, '2015-05-24 17:00:00', 'Borshch', 'CSE Atrium', 'Who wants to eat traditional Ukranian food?');
INSERT INTO MealPlan VALUES (2, 4, '2015-05-21 19:00:00', 'Vietnamese Pho', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (3, 8, '2015-05-22 17:30:00', 'Indian', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (4, 6, '2015-05-22 18:30:00', 'Chipotle', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (5, 7, '2015-05-24 18:30:00', 'Fried Chicken', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (6, 3, '2015-05-24 18:30:00', 'Udon', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (7, 10, '2015-05-24 19:30:00', 'Bubble Tea', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (8, 5, '2015-05-26 12:30:00', 'Subway', 'CSE Atrium', 'this is a description');
INSERT INTO MealPlan VALUES (9, 9, '2015-05-26 16:30:00', 'Thai Tom', 'CSE Atrium', 'this is a description');

-- Some attendees to work with
INSERT INTO Attending VALUES (1, 1);
INSERT INTO Attending VALUES (1, 2);
INSERT INTO Attending VALUES (1, 3);
INSERT INTO Attending VALUES (2, 4);
INSERT INTO Attending VALUES (2, 8);
INSERT INTO Attending VALUES (3, 8);
INSERT INTO Attending VALUES (4, 6);
INSERT INTO Attending VALUES (5, 7);
INSERT INTO Attending VALUES (6, 3);
INSERT INTO Attending VALUES (7, 10);
INSERT INTO Attending VALUES (8, 5);
INSERT INTO Attending VALUES (9, 9);
