<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will get meal plan details for plans matching
 * the given query arguments
 */

$query = "SELECT m.id, m.hostid, m.time, m.foodmood, m.location, m.description, a.name
          FROM MealPlan m JOIN Account a on a.id = m.hostid";
$whereArgs = array();

if (isset($_GET["id"])) {
  array_push($whereArgs, "m.id = " . $_GET["id"]);
}

if (isset($_GET["hostid"]) && strlen($_GET["hostid"]) != 0) {
  array_push($whereArgs,"m.hostid = " . $_GET["hostid"]);
}

if (isset($_GET["gender"]) && strlen($_GET["gender"]) != 0) {
  array_push($whereArgs,"a.gender = " . $db->quote($_GET["gender"]));
}

if (isset($_GET["year"]) && strlen($_GET["year"]) != 0) {
  if ($_GET["year"] == "7") {
    array_push($whereArgs,"a.year <= 4");
  } else {
    array_push($whereArgs,"a.year = " . $_GET["year"]);
  }
}

if (isset($_GET["time"])) {
  array_push($whereArgs, "m.time >= " . $db->quote($_GET["time"]));
} else if (!isset($_GET["id"])) {
  $now = date("Y-m-d H:i:s", time() - 60*10);   // set it to ten minutes ago
  array_push($whereArgs, "m.time >= " . $db->quote($now));
}

if (isset($_GET["major"])) {
  array_push($whereArgs,"a.major LIKE " . $db->quote("%" . $_GET["major"] . "%"));
}

if (isset($_GET["hostname"])) {
  array_push($whereArgs,"a.name LIKE " . $db->quote("%" . $_GET["hostname"] . "%"));
}

if (isset($_GET["foodmood"])) {
  array_push($whereArgs, "m.foodmood LIKE " . $db->quote("%" . $_GET["foodmood"] . "%"));
}

if (isset($_GET["location"])) {
  array_push($whereArgs, "m.location LIKE " . $db->quote("%" . $_GET["location"] . "%"));
}

if (isset($_GET["description"])) {
  array_push($whereArgs, "m.description LIKE " . $db->quote("%" . $_GET["description"] . "%"));
}

if (count($whereArgs) > 0) {
  $query .= " WHERE " . $whereArgs[0];
  for ($i = 1; $i < count($whereArgs); $i++) {
    $query .= " AND " . $whereArgs[$i];
  }
}

$query .= " ORDER BY time;";

// Get the plans
$rows = $db->query($query);

// Reaching here means query was successful
$response["success"] = 1;
$response["results"] = array();
  
foreach ($rows as $row) {
  $mealplan = array();
  $mealplan["id"] = $row["id"];
  $mealplan["hostid"] = $row["hostid"];
  $mealplan["hostname"] = $row["name"];
  $mealplan["time"] = $row["time"];
  $mealplan["foodmood"] = $row["foodmood"];
  $mealplan["location"] = $row["location"];
  $mealplan["description"] = $row["description"];
  array_push($response["results"], $mealplan);
}

// echoing JSON response
echo json_encode($response);

?>
