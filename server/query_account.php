<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will get account information of the given user, creating
 * a new entry if the email isn't in the database
 */

$query = "SELECT *
          FROM Account
          WHERE ";

if (isset($_GET["id"])) {
  $id = $_GET["id"];
  $query .= "id = $id;";
} else if (isset($_GET["email"])) {
  $email = $db->quote($_GET["email"]);
  $query .= "email = $email;";
} else {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

$rows = $db->query($query);
$rows = $rows->fetchAll();  // Get all the rows in an array

// Create new user if they don't exist
if (count($rows) == 0) {
  // create new user and get the default information
  $statement = "INSERT INTO Account (email) VALUES($email);";
  $db->exec($statement, 1);
  
  // Success getting here, try getting user information now
  $rows = $db->query($query);
}

// Success reaching here
$response["success"] = 1;
$response["results"] = array();

// Get the user information 
foreach ($rows as $row) {
  $account = array();
  $account["id"] = $row["id"];
  $account["email"] = $row["email"];
  $account["name"] = $row["name"];
  $account["year"] = $row["year"];
  $account["gender"] = $row["gender"];
  $account["major"] = $row["major"];
  array_push($response["results"], $account);
}

// echoing JSON response
echo json_encode($response);

?>