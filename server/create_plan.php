<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will create a new meal plan row
 * All meal plan details are read from HTTP Post Request
 */

if (!isset($_POST['hostid']) || !isset($_POST['time']) || !isset($_POST['foodmood']) 
    || !isset($_POST['location']) || !isset($_POST['description'])) {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

// Read in parameters, escaping what would be user input
$hostid = $_POST['hostid'];
$time = $_POST['time'];
$foodmood = $db->quote($_POST['foodmood']);
$location = $db->quote($_POST['location']);
$description =$db->quote( $_POST['description']);

$statement = "INSERT INTO MealPlan(hostid, time, foodmood, location, description) 
              VALUES('$hostid', '$time', $foodmood, $location, $description);";
              
// Insert the new plan
$result = $db->exec($statement, 1);

// Reaching here means insert was successful
$response["success"] = 1;
$response["message"] = "Plan successfully created.";
$response["id"] = $db->lastInsertId();

// echoing JSON response
echo json_encode($response);
?>