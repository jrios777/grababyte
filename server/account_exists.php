<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code tests to see if given email exists, prints
 true if so, false otherwise.
 */

$query = "SELECT *
          FROM Account
          WHERE ";

if (isset($_GET["email"])) {
  $email = $db->quote($_GET["email"]);
  $query .= "email = $email;";
} else {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

$rows = $db->query($query);
$rows = $rows->fetchAll();

// Create new user if they don't exist
if (count($rows) == 0) {
  echo "false";
} else {
  echo "true";
}
?>