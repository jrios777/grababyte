<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will remove someone who was attending or hosting a plan
 * and reassign the host if needed
 */

if (!isset($_POST["planid"]) || !isset($_POST["attendeeid"])) {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

// Read in parameters
$planid = $_POST["planid"];
$attendeeid = $_POST["attendeeid"];

// Get the plan's hostid
$query = "SELECT hostid FROM MealPlan where id = $planid;";
$plan = $db->query($query)->fetch();


if ($plan["hostid"] != $attendeeid) {
  // Not the host, remove the attendee
  $statement = "DELETE FROM Attending
                WHERE planid = $planid AND attendeeid = $attendeeid;";
  $result = $db->exec($statement, 1);
  
  // successfully removed from database
  $response["success"] = 1;
  $response["message"] = "Attendee successfully removed.";
} else {
  // We're removing the host, get the attendees
  $query = "SELECT * FROM Attending WHERE planid = $planid;";
  $attendees = $db->query($query)->fetchAll();
  
  if (count($attendees) == 0) {
    // No attendees, remove the plan
    $statement = "DELETE FROM MealPlan WHERE id = $planid;";
    $db->exec($statement, 1);
    
    // successfully removed plan from the database
    $response["success"] = 1;
    $response["message"] = "Host and plan successfully removed.";
  } else {
    // There are attendees, replace the hostid with an attendee's id
    $nextHostId = $attendees[0]["attendeeid"];
    $statement = "UPDATE MealPlan SET hostid = $nextHostId WHERE id = $planid;";
    $db->exec($statement, 1);
    
    // Remove from the attending table the new host
    $statement = "DELETE FROM Attending WHERE planid = $planid AND attendeeid = $nextHostId;";
    $db->exec($statement, 1);
    
    // successfully removed plan from the database
    $response["success"] = 1;
    $response["message"] = "Successfully reassigned host and removed their attending entry.";
  }
}

echo json_encode($response);
?>