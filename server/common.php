<?php
header("Content-type: application/json");

// include db connect class
require_once __DIR__ . '/db_connect.php';

// define error constants
$ERROR_HTTP = -2;    // Bad Http Request, missing arguments
$ERROR_DBCONN = -3;  // Database connection error
$ERROR_SQL = -4;     // SQL Statement execution error

// function for connecting to the db
function db_connect() {
  global $db;
  $db = new DB_CONNECT();
}

// array for JSON response
$response = array();



/*
  Encodes an error in a JSON object, echoes it and makes the page die. 
  Arguments are as follows:
  errno: the error number, where
         -2 - Missing Parameters in Http Request
         -3 - DB connection error
         -4 - Malformed SQL statements due to invalid arguments
  exception: the PDO::exception that may have been thrown
  statement: the SQL statement that failed
*/
function error_encode($errno, $exception = NULL, $statement = NULL) {
  // array for JSON response
  $response = array();
  $response["success"] = "0";
  if ($errno == -2) {
    $response["message"] = "Missing arguments, couldn't process request.";
  } else if ($errno == -3) {
    $response["message"] = "Database connection error. Exception: " . $exception->getMessage();
  } else if ($errno == -4) {
    $response["message"] = "Couldn't process the database request. Exception: " . 
    $exception->getMessage() . ", SQL Statement: " . $statement;
  } else {
    $response["message"] = "Exception: " . $exception->getMessage();
  }
  $response["errno"] = $errno;
  echo json_encode($response);
  die();
} ?>