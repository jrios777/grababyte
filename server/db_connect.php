<?php

/**
 * This class file is a wrapper around the PDO database handler. It implements
 * all methods used in other PHP files but includes exception handling so that
 * errors are printed out in JSON object format.
 */
class DB_CONNECT {
    private $db;
 
    // constructor
    function __construct() {
        // connecting to database
        $this->connect();
    }
 
    // destructor
    function __destruct() {
        // closing db connection
        $this->close();
    }
 
    // Initializes connection with the database, echoes a JSON object with
    // failure information on failure
    function connect() {
        // import database connection variables
        require_once __DIR__ . '/db_config.php';
        global $ERROR_DBCONN;   // defined in common.php
 
        // Connecting to mysql database
        try {
          // Each of the following constants are defined in db_config.php
          $db = new PDO('mysql:host=' . DB_SERVER .';dbname=' . DB_DATABASE, DB_USER, DB_PASSWORD);
        } catch (PDOException $e) {
          error_encode($ERROR_DBCONN, $e);
        }
        
        // Set the database handler to throw exceptions on error
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

        // returing connection cursor
        $this->db = $db;
    }
 
    // Closes the db connection by setting it to null
    function close() {
        // closing db connection
        $this->db = NULL;
    }
    
    // The following 4 functions do exactly as PDO, only handling errors by printing
    // in JSON format so that the android app has no problem handling the response
    
    function quote($value) {
      return $this->db->quote($value);
    }
    
    function query($query) {
      global $ERROR_SQL;  // defined in common.php
      try {
        return $this->db->query($query);
      } catch (PDOException $e) {
        error_encode($ERROR_SQL, $e, $query);
      }
    }
    
    // This is slightly different, it takes in an additional argument for 
    // checking to make sure the number of rows is as expected. By default
    // this is null, but should be set to the number of rows if other code
    // expects a specific number of rows to be affected.
    function exec($statement, $expected = NULL) {
      global $ERROR_SQL;  // defined in common.php
      try {
        $rowsAffected = $this->db->exec($statement);
      } catch (PDOException $e) {
        error_encode($ERROR_SQL, $e, $statement);
      }
      if ($expected != NULL && $rowsAffected != $expected) {
        error_encode($ERROR_SQL, $e, "Expected to affect $expected rows, only
        affected $rowsAffected rows.");
      }
    }
    
    function lastInsertId() {
      return $this->db->lastInsertId();
    }
}
 
?>
