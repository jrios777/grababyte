<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will get the list of attendees for a plan or the list of plans
 * a user is attending.
 */

if (isset($_GET["planid"])) {
  // Get the attendees for this plan
  $planid = $_GET["planid"];
  $query = "SELECT attendeeid, a.name AS name, a.major AS major, 
            a.email AS email, a.gender AS gender, a.year AS year
            FROM MealPlan m 
            JOIN Attending ON m.id = planid
            JOIN Account a ON a.id = attendeeid
            WHERE planid = $planid;";
} else if (isset($_GET["attendeeid"])) {
  // Get the plans this attendee is attending
  $attendeeid = $_GET["attendeeid"];
  $datetime = $db->quote(date("Y-m-d H:i:s", time() - 60*10));  // 10 mins ago
  $query = "SELECT m.id AS id, m.hostid AS hostid, a.name AS hostname, m.time AS time,
            m.foodmood AS foodmood, m.location AS location, m.description AS description
            FROM MealPlan m
            JOIN Attending ON id = planid
            JOIN Account a ON a.id = m.hostid
            WHERE attendeeid = $attendeeid
            AND m.time >= $datetime
            ORDER BY m.time;";
} else {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

$query .= " ORDER BY time;";

// Find the results
$rows = $db->query($query);

// This means query was successful
$response["success"] = 1;
$response["results"] = array();

if (isset($_GET["planid"])) {
  foreach ($rows as $row) {
    $attendant = array();
    $attendant["id"] = $row["attendeeid"];
    $attendant["name"] = $row["name"];
    $attendant["major"] = $row["major"];
    $attendant["year"] = $row["year"];
    $attendant["gender"] = $row["gender"];
    $attendant["email"] = $row["email"];
    array_push($response["results"], $attendant);
  }
} else {
  foreach ($rows as $row) {
    $mealplan = array();
    $mealplan["id"] = $row["id"];
    $mealplan["hostid"] = $row["hostid"];
    $mealplan["hostname"] = $row["hostname"];
    $mealplan["time"] = $row["time"];
    $mealplan["foodmood"] = $row["foodmood"];
    $mealplan["location"] = $row["location"];
    $mealplan["description"] = $row["description"];
    array_push($response["results"], $mealplan);
  }
}

// echoing JSON response
echo json_encode($response);

?>
