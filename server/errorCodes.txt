The JSON objects returned by the PHP returns in this format,
On SUCCESS:
{
  "success": 1
  "message": "this exists if the request is a POST request, not in a GET request"
  ...
}

On ERROR:
{
  "success": 0
  "message": "..."
  "errno": #
}

The errno ties in with the values logged by DBManager.java

Errno   Description                         Notes
-2      Bad Request - Missing parameters    
-3      Unable to connect to the database   
-4      Internal SQL execution error        
-5      Internal PHP execution error        When connection response code is 500
-6      Unknown Error 

These are the JSON error numbers that the PHP server can return:

