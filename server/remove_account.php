<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will remove a user's information from the database, and 
 * reassign hosts toany plans they were hosting. The database has a constraint
 * to remove any attending entries when attendee is deleted and remove any
 * plan entries when host is deleted
 */

if (!isset($_POST["id"])) {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}
// Read in the account id
$id = $_POST["id"];

// What plans is this user hosting?
$query = "SELECT id FROM MealPlan WHERE hostid = $id;";
$hostingPlans = $db->query($query);

// For each plan, reassign the host if there's any attendees
foreach ($hostingPlans as $row) {
  $planid = $row["id"];
  $query = "SELECT attendeeid FROM Attending WHERE planid = $planid;";
  $attendees = $db->query($query)->fetchAll();
  if (count($attendees) > 0) {
    // There are attendees, replace the hostid with an attendee's id
    $nextHostId = $attendees[0]["attendeeid"];
    $statement = "UPDATE MealPlan SET hostid = $nextHostId WHERE id = $planid;";
    $db->exec($statement, 1);
    
    // Remove from the attending table the new host
    $statement = "DELETE FROM Attending WHERE planid = $planid AND attendeeid = $nextHostId;";
    $db->exec($statement, 1);
  }
  $count = count($attendees);
}

// Now that the user isn't a host for any plans, delete them
$statement = "DELETE FROM Account WHERE id = $id;";
$db->exec($statement, 1); // TODO, CHECK

// successfully deleted from database, deleting one row
$response["success"] = 1;
$response["message"] = "Account successfully removed.";

echo json_encode($response);
?>