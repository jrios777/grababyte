# Server Side Source Code

## About this directory
This directory contains:
- PHP code that runs on the back end and produces JSON objects returned to the Android application.
- HTML code that can be pulled up on the back end to test PHP file functionality. 

## What this directory does NOT contain
- Private key for our EC2 instance
- Private db\_config.php that contains sensitive information for our RDS instance
