<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will insert an attendee for a specified plan
 */

if (!isset($_POST["planid"]) || !isset($_POST["attendeeid"])) {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

// Read in parameters
$planid = $_POST["planid"];
$attendeeid = $_POST["attendeeid"];

$statement = "INSERT INTO Attending
          VALUES($planid, $attendeeid);";
          
// Insert the new attending entry
$result = $db->exec($statement, 1);

$response["success"] = 1;
$response["message"] = "Attendee successfully added.";
$response["id"] = "0";

echo json_encode($response);
?>