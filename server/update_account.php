<?php
include("common.php");  // Common setup and error handling
db_connect();   // Connect to db
 
/*
 * Following code will update account information
 */

if (!isset($_POST['id']) && !isset($_POST['name']) && !isset($_POST['year']) && 
    !isset($_POST['gender']) && !isset($_POST['major'])) {
  // bad http request, missing arguments
  error_encode($ERROR_HTTP);
}

// Read in parameters, escaping what would be user input
$id = $_POST['id'];
$year = $_POST['year'];
$email = $db->quote($_POST['email']);
$name = $db->quote($_POST['name']);
$gender = $db->quote($_POST['gender']);
$major = $db->quote($_POST['major']);

// Update account information
$statement = "UPDATE Account 
              SET name = $name, year = $year, gender = $gender, major = $major 
              WHERE id = $id;";
$db->exec($statement, 1);

// successfully inserted into database
$response["success"] = 1;
$response["message"] = "Account successfully updated.";

// echoing JSON response
echo json_encode($response);
?>