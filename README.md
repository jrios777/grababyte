# Hello and welcome to our CSE403 repository!

## Guides

Looking for the User Guide? Refer to the [product website](http://homes.cs.washington.edu/~jrios777/cse403/web/product).

Looking for the Developer Guide? This file has setup and development instructions. For more details refer to the [developer website](http://homes.cs.washington.edu/~jrios777/cse403/web/). 

Looking for the Help Page/FAQ? If the Google+ Sign Button does not work, or you have any other questions, refer to the [help page](http://homes.cs.washington.edu/~jrios777/cse403/web/product/help.html). 

### Development Tools

The following tools are used, click the links to download and install if you don't already have them installed. 
We also recommend using Mac/Windows for the Android Studio instructions because Linux may have a lot of missing tools and may not match exactly
(See section on How to Setup an Automated Daily Build and Test for an example of what needed to be installed on a remote linux machine).
- [Java 7 JDK or better](http://www.oracle.com/technetwork/java/javase/downloads/index.html) - For Java development
- [Android Studio](https://developer.android.com/sdk/index.html) - For Android development
  - On the CSE Linux machines you may need to install binaries for running 32-bit tools by typing the following
    - `sudo yum install glibc.i686 zlib.i686 libstdc++.i686 ncurses-libs.i686 libgcc.i686`
- [Genymotion](https://www.genymotion.com/#!/download) - Not required, but it is a better, faster emulator for Android
- [VirtualBox](https://www.virtualbox.org/wiki/Downloads) - Genymotion's virtual machine

### How to Obtain the Source Code

1. In the terminal, `cd` into the directory where you'd like to copy the source
2. Type `git clone https://gitlab.cs.washington.edu/jrios777/cse403.git` to get the source code.

The source code repository can be found [here](https://gitlab.cs.washington.edu/jrios777/cse403).

### Importing the Android project into Android Studio
1. Open Android Studio
  1. If prompted with the welcome screen, select "Import project (Eclipse ADT, Gradle, etc.)"
  2. If not, go to File -> Import Project
2. Find the folder where the repo is and select the MeetAGenius folder
3. You can be sure you imported this correctly if you can locate in the project explorer the source files.
4. If the build fails, see the Android Studio Plugins section

### Android Studio Plugins
#### Lombok
We use this tool to remove redundant boilerplate code you often find in data classes such as getters, setters, all args constructors, etc.
- To use, go to Android Studio -> Preferences -> Plugins -> Browse repositories... -> *Search* Lombok Plugin -> Install Plugin
- [More on Lombok](https://projectlombok.org/index.html)

#### RetroLambda
We use this tool to simplify statements using Java 8 lambda functions. As an example, consider the following code:
<pre>
...
button.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {
        new LoadTask().execute();
    }
});
...
</pre>
Using Java 8 / Retrolambda, this code simply becomes
<pre>
...
button.setOnClickListener(v -> new LoadTask().execute());
...
</pre>
- To use, install [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/index.html) on your machine, go to File -> Project Structure -> SDK Location and set JDK location to the location of your Java 8 JDK

### Directory Structure
- TutorialOutputs -Folder containing code or files from output of assigned tutorials. This includes version control tutorials and android tutorials that members tried to complete, but needed help with debugging.
- Feedback -Folder containing feedback from TAs on assignments.
- MeetAGenius -Folder for actual app.
  - app/src/main -Folder for main source code.
    - AndroidManifest.xml -Manifest file.
    - java/edu/uw/cs/meetagenius -Folder containing main java files.
  - app/src/androidTest -Folder containing testing code
    - java/edu/uw/cs/meetagenius -Folder containing java testing files.
- references -Folder containing useful references such as sample code and troubleshooting files.
  - aws-sdk-android-samples -Folder containing various examples/projects for aws sdk for android to use for reference if needed.
  - troubleshoot -Folder containing files that help people experiencing trouble with external softwares/tools such as AndroidStudio or Genymotion.
- documents -Folder containing documents that will be turned in. Each assignment has its own folder (e.g. “Proposal”, “Architectural Design”) except for Weekly Status Reports, which are grouped together in the folder “WeeklyReports”.
- meeting -Folder containing meeting notes and agendas.
- web -Folder containing files required for website.
- pushWeb.sh -Script for pushing the web folder onto the project website. Used by Josue and the automated build
- server -Folder containing the PHP server files and initial database SQL statements.
- README.md -File containing useful instructions for how to get started on the project.

### How to Build the Software:

#### Prerequisites: Install the following from the SDK Manager
- Android SDK Platform-tools
- Android SDK Build-tools
- SDK Platform Android 5.1
- SDK Platform Android 5.0
- Android Support Repository
- Android Support Library

These may be installed in either of the following ways:
- In Android Studio select and download from the SDK Manager by going to Tools -> Android -> SDK Manager
- In the Linux terminal, you can refer to steps 5.5-5.8 in "How to Setup an Automated Daily Build and Test" below.

#### In General 
- In the Android Studio top menu bar, select Build -> Make Project.
- In the Terminal `cd` into the Android project and run `./gradlew build`
- NOTE: Running the app from Android Studio may not allow you to log in with Google. We will need the SHA1 Fingerprint of your debug keystore to allow it to work. Or you can it up [here](https://developers.google.com/+/mobile/android/sign-in)

#### Developers on the Team also have the following option to build and create the binaries
1. Go here: http://ec2-52-11-250-252.us-west-2.compute.amazonaws.com:8080
2. Click cse403-upload
3. Click Build Now on the left
4. Click the newest build number on the left
5. Click console output
6. If the build was successful, the apk will be updated at http://homes.cs.washington.edu/~jrios777/cse403/web/apk/app-debug.apk

### How to Test the Software
- From Android Studio: navigate to app/src/androidTest/java/edu/uw/cs/meetagenius/FullTestSuite.java in Android Studio, right click and press Run->(Android icon).
- From the terminal, `cd` into the Android project and run `./gradlew test`

### How to Check the Test Coverage
- From Android Studio 1.2 or later: right click on the FullTestSuite (Under app/src/test/java), and select configuration.
- Build the test as a regular JUnit test.
- Click the Code Coverage button at the top of Android Studio, near the normal Run button.
- Code coverage will be listed along the right-hand side.
- Android Studio will automatically prompt you to generate a convenient html report. 
- If desired, simply accept and choose the desired configurations for the report.
- **[Latest report HERE](http://homes.cs.washington.edu/~jrios777/cse403/web/coverage/index.html)**

### How to Setup an Automated Daily Build and Test
This is the way we set up our automated daily build/test, which you can do as well:
1. Sign Up for / Login in to an AWS account [here](http://aws.amazon.com)
2. Launch an EC2 instance with the following settings:
  - Running Red Hat Enterprise Linux
  - t2.micro instance type (once here you can jump to Review on their page and click Launch)
  - Be sure to store the Key Value Pair private key for reference
3. Go back to the EC2 dashboard and store public DNS name of your instance somewhere for reference as well
4. In the terminal, type `ssh -i <key-pair(.pem)> ec2-user@<public_dns_name>` replacing the careted values with the key file and dns name you saved.
5. Now that you're in your cloud instance, type the following commands to set up the tools you'll need:
  1. `sudo yum install -y java-1.7.0-openjdk-devel` - To get the Java Development Kit for Linux
  2. `sudo yum install glibc.i686 zlib.i686 libstdc++.i686 ncurses-libs.i686 libgcc.i686` - To get the libraries/tools needed for the 32-bit binaries android tools may have.
  3. Get Jenkins, our build/test automator. Type the following:  
        - `sudo curl -o /etc/yum.repos.d/jenkins.repo http://pkg.jenkins-ci.org/redhat/jenkins.repo` - to download Jenkins
        - `sudo rpm --import https://jenkins-ci.org/redhat/jenkins-ci.org.key` - to download key for Jenkins
        - `sudo yum install jenkins` - to install Jenkins
        - `sudo service jenkins start` - to start Jenkins
  4. `sudo yum install git` - needed to access the source repository
  5. In your home directory, type `curl -O http://dl.google.com/android/android-sdk_r24.1.2-linux.tgz` to get the Android SDK.
  6. `tar xvf android-sdk_r24.1.2-linux.tgz` will unpack the sdk folder.
  7. `cd android-sdk-linux/tools` and type `./android list sdk` and see which numbers are the following:
        - Android SDK Platform-tools
        - Android SDK Build-tools
        - SDK Platform Android 5.1
        - SDK Platform Android 5.0
        - Android Support Repository
        - Android Support Library
  8. Type `./android update sdk -u -t #,#,#,#` where the comma separated '#' marks are a comma separated list of numbers you found you need from the list above.
  9. `cd ~` and run the command `git clone https://gitlab.cs.washington.edu/jrios777/cse403.git` to get the repo
  10. `cd ~/cse403/MeetAGenius` and create a file called `local.properties` writing inside the line `sdk.dir=/home/ec2-user/android-sdk-linux/`
  11. Your tools are now ready. 
6. Run `./gradlew build` to build the application. If you see lint warnings that cause the build to fail you can turn those off in the `app/build.gradle` file by adding the following inside of the android {}: `lintOptions { abortOnError false }`
7. Back in your AWS EC2 dashboard, click Security Groups, select the one that was automatically for this instance, and select Actions -> Edit Inbound Rules.
8. Add and Save a new Rule with the following properties:
  - Type: Custom TCP Rule
  - Protocol: TCP
  - Port Range: 8080
  - Source: Anywhere (You can set this to something else if you'd like to restrict access to Jenkins by IP address)
9. Refer to your instance's public dns name and go to it on the web browser, `http://<your-dns-name>:8080`
10. From there do the following to set up the automated build:
  - Go to Manage Jenkins -> Manage Plugins -> Available -> Search/Download GitLab plugin.
  - Click New Item
  - Select FreeStyle Project, give a name, and click OK
  - You'll be taken a build configurations page, do the following:
    - Provide the git repository info under Source Code Management
    - Under Build Triggers, select Build periodically and give it a schedule (Use ? marks on the side for help) to setup automated daily builds.
    - Under Build, Provide a shell script for building, which may look something like this:
<pre>
\#!/bin/sh
\# Get the current copy of the repo
if [ -d "cse403" ]; then
  # Control will enter here if $DIRECTORY exists.
  git clone https://gitlab.cs.washington.edu/jrios777/cse403.git
  echo just cloned the git repo
fi\n
\# Change into the project directory
cd MeetAGenius\n
\# Adds where the SDK is to the local properties (You may have to change the permissions so that jenkins can read/write/run from the sdk folder)
echo "sdk.dir=/home/ec2-user/android-sdk-linux/" > local.properties\n
\# build, test and create the application
./gradlew build | tee latestBuild.txt\n
result=`tail -n 3 latestBuild.txt | head -n 1`
if [ "$result" != "BUILD SUCCESSFUL" ]; then
  exit 1  # indicates failure on build
fi
</pre>
    - Under Post-build Actions, you may set up email notifications (we did) but you'll have to change system configurations and look into Amazon SES [here](http://docs.aws.amazon.com/ses/latest/DeveloperGuide/getting-started.html).
    - When you're happy with your settings click Save.
11. Now you're set to go with automatic builds.

Note: We've left out instructions we used to email everyone on a failed build and update the website on successful builds for the sake of brevity but those will require setting up Amazon SES and using the ssh private key
  
### Release a New Version of software:
1. The first thing that should be done when releasing a new version of the software is to update the release version number in the /app/Gradle Scripts/build.gradle file. 
2. The next step is to push the code to the git repo. 
3. Simply follow the instructions in “How to Build the Software”. This will both run the tests and create the binaries. 
   - If you're on our team, it will update our website automatically through the script we use when building on Jenkins.
   - Technically all releases on the website are testing releases, for the public, official release refer to the Google Play Store guidelines for app publishing. An alternative method for keeping the website apk official is to simply rename the file and do any necessary changes for the script for when you want to create only a test build, or a full build.

### Viewing Current and Resolved Bugs
Our project uses the GitLab Issue Tracker to manage bugs. 
To view current bugs and bug history, use the URL: https://gitlab.cs.washington.edu/jrios777/cse403/issues. 
To create bugs, modify the status of bugs, or resolve bugs, you must join the project first. 
Once you have joined the project, you can create a bug by clicking on the green “New Issue” button located on the 
upper right corner. To resolve a bug, you click on its title and then press the “Close” button, which is also on 
the upper-right corner.