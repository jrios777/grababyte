<!DOCTYPE html>
<html>
  <head>
    <title>Chat History</title>
    <link rel="stylesheet" type="text/css" href="bootstrap/css/bootstrap.css" />
    <link rel="stylesheet" type="text/css" href="history.css" />
  </head>
  <body>
    <div>
    <h1>Chat History</h1>
    <h2>Using Telegram</h2>
    <p>
    <?php
        $history = file("history.txt");
        $members = array("Alice", "Jessica Li", "Josue Rios", "Junhao", "Kaleo Brandt", "Michelle Cho", "Tad", "Thao Nguyen Dang");
        foreach ($history as $line) {
          if ($line[0] == "-") {
            continue;
          } elseif ($line[0] == "[") {
            # This is something to print out
            $day = substr($line, 1, 2);
            $month = substr($line, 4, 3);
            $date = $month . " " . $day;
            $action = explode("»»»", substr($line, 17));
            if (count($action) > 1) {
              $heading = $action[0] . " on " . $date . " :";
              $info = $action[1];
            } else {
              $heading = "On " . $date . " :";
              $info = "<em>" . $action[0] . "</em>";
            }
            
            foreach ($members as $member) {
              if (strpos($line, $member) == 17) {
                $name = strtolower(implode("-",explode(" ",$member)));
                break;
              }
            }
            
            
            ?>
              </p>
              </div>
              <div class="<?= $name ?>">
              <h4><?= $heading ?></h4>
              <p>
                <?= $info ?>
    <?php } else { ?>
              <br /><?= $line ?>
    <?php }
        } ?> 
    </p>
    </div>
  </body>
</html>