5.22 Feature Complete Release CheckList

1. Repo: All major functionality should be present and working correctly
   Only exception: error-handling can wait [Better not]

2. Complete User Documentation

3. Up-to-date SRS

4. Up-to-date SDS

5. Up-to-date UML Diagrams

6. Up-to-date Developer Documentation

7. Tests! Thoroughness is important.

8. Test coverage tool and description
   Need execution record of this coverage tool

9. Code style and performance optimization

A. Fill in ChangeList.doc for recent changes start from beta release