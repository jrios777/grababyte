-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:1:1
	xmlns:android
		ADDED from AndroidManifest.xml:1:11
	package
		ADDED from AndroidManifest.xml:2:5
	android:versionName
		ADDED from AndroidManifest.xml:4:5
	android:versionCode
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-sdk
ADDED from AndroidManifest.xml:6:5
MERGED from com.facebook.android:facebook-android-sdk:3.20.0:20:5
MERGED from com.android.support:support-v4:20.0.0:16:5
MERGED from com.google.android.gms:play-services:6.1.71:15:5
MERGED from com.android.support:support-v4:20.0.0:16:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:8:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:7:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.GET_ACCOUNTS
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.USE_CREDENTIALS
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
application
ADDED from AndroidManifest.xml:15:5
MERGED from com.facebook.android:facebook-android-sdk:3.20.0:24:5
MERGED from com.android.support:support-v4:20.0.0:17:5
MERGED from com.google.android.gms:play-services:6.1.71:16:5
MERGED from com.android.support:support-v4:20.0.0:17:5
	android:label
		ADDED from AndroidManifest.xml:18:9
	android:allowBackup
		ADDED from AndroidManifest.xml:16:9
	android:icon
		ADDED from AndroidManifest.xml:17:9
	android:theme
		ADDED from AndroidManifest.xml:19:9
activity#com.amazonaws.cognito.sync.demo.MainActivity
ADDED from AndroidManifest.xml:20:9
	android:label
		ADDED from AndroidManifest.xml:22:13
	android:name
		ADDED from AndroidManifest.xml:21:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:23:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:24:17
	android:name
		ADDED from AndroidManifest.xml:24:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:26:17
	android:name
		ADDED from AndroidManifest.xml:26:27
activity#com.amazonaws.cognito.sync.demo.ListDatasetsActivity
ADDED from AndroidManifest.xml:29:9
	android:configChanges
		ADDED from AndroidManifest.xml:30:13
	android:name
		ADDED from AndroidManifest.xml:29:19
activity#com.amazonaws.cognito.sync.demo.ListRecordsActivity
ADDED from AndroidManifest.xml:31:9
	android:configChanges
		ADDED from AndroidManifest.xml:32:12
	android:name
		ADDED from AndroidManifest.xml:31:19
activity#com.amazonaws.cognito.sync.demo.EditRecordActivity
ADDED from AndroidManifest.xml:33:9
	android:configChanges
		ADDED from AndroidManifest.xml:34:13
	android:name
		ADDED from AndroidManifest.xml:33:19
activity#com.facebook.LoginActivity
ADDED from AndroidManifest.xml:35:9
	android:label
		ADDED from AndroidManifest.xml:37:13
	android:name
		ADDED from AndroidManifest.xml:36:13
activity#com.amazon.identity.auth.device.authorization.AuthorizationActivity
ADDED from AndroidManifest.xml:38:9
	android:theme
		ADDED from AndroidManifest.xml:42:13
	android:allowTaskReparenting
		ADDED from AndroidManifest.xml:40:13
	android:name
		ADDED from AndroidManifest.xml:39:13
	android:launchMode
		ADDED from AndroidManifest.xml:41:13
intent-filter#android.intent.action.VIEW+android.intent.category.BROWSABLE+android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:43:13
action#android.intent.action.VIEW
ADDED from AndroidManifest.xml:44:17
	android:name
		ADDED from AndroidManifest.xml:44:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:46:17
	android:name
		ADDED from AndroidManifest.xml:46:27
category#android.intent.category.BROWSABLE
ADDED from AndroidManifest.xml:47:17
	android:name
		ADDED from AndroidManifest.xml:47:27
data
ADDED from AndroidManifest.xml:49:17
	android:host
		ADDED from AndroidManifest.xml:50:21
	android:scheme
		ADDED from AndroidManifest.xml:51:21
meta-data#com.facebook.sdk.ApplicationId
ADDED from AndroidManifest.xml:55:9
	android:name
		ADDED from AndroidManifest.xml:56:13
	android:value
		ADDED from AndroidManifest.xml:57:13
meta-data#com.google.android.gms.version
ADDED from AndroidManifest.xml:58:9
	android:name
		ADDED from AndroidManifest.xml:58:20
	android:value
		ADDED from AndroidManifest.xml:58:66
