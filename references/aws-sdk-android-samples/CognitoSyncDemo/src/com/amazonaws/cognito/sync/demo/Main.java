package com.amazonaws.cognito.sync.demo;

import android.content.ContentValues;

import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import com.mysql.jdbc.Driver;

/**
 * Created by jrios777 on 5/4/15.
 */
public class Main {

    public static void main(String[] args) {
        System.out.println("Hello World");

        try {
            connectToAndQueryDatabase("aws_meetagenius", "passagenius");
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    public static void connectToAndQueryDatabase(String username, String password) throws SQLException {

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (Exception e) {

        }
        Connection con = DriverManager.getConnection(
                "jdbc:mysql://magappdb.ci0eek3nrlg9.us-west-2.rds.amazonaws.com:3306/magappdb",
                username,
                password);

        Statement stmt = con.createStatement();
        ResultSet rs = stmt.executeQuery("SELECT name, time, foodmood FROM MealPlan m JOIN Account a on m.hostId = a.id;");
        System.out.printf("%s\t%s\t%s\n", "name", "time", "foodmood");

        while (rs.next()) {
            String hostId = rs.getString("name");
            Date d = rs.getDate("time");
            String mood = rs.getString("foodmood");
            System.out.printf("%s\t%s\t%s\n", hostId.toString(), d.toString(), mood);
        }
    }
}
