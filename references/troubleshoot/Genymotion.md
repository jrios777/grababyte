# Genymotion
###### Purpose
To provide solutions to problems encountered when dealing with Genymotion.

### Problem 1: Installing and Running Genymotion
1. Download and Install [Genymotion](https://www.genymotion.com/#!/download)
2. Download and Install [VirtualBox](https://www.virtualbox.org/wiki/Downloads)
3. Install the Genymotion Plugin for Android Studio by going to Preferences -> Plugins -> Browse Repositories -> Search "Genymotion" -> Install plugin
4. Create a Genymotion device by opening the Genymotion _app_ on your computer (not the device manager in Android Studio) and click Add -> (Select a device, with version 5.0 preferrably) -> Next -> Next
5. Start the device by clicking on "Start" in the Genymotion app.
6. Run your app, the Genymotion device should now appear as a running device
