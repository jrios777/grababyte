# Troubleshooting

### Purpose
The purpose of this folder is for us to write down any solutions we find to problems with experience with Android Studio, Genymotion or any other tool we're all using

### How you can use this
If you're having issues with some tool, take a look at the corresponding .md for the tool (if it exists). If it doesn't exist, try figuring it out, and when you do add it to the corresponding .md file. If you still can't figure it out, add it to the issue tracker and assign it to someone whose issue it seems to be.

### Examples
I've created two files called "Genymotion.md" and "AndroidStudio.md" that have some solutions to problems I've some of us face. Feel free to modify these files on GitLab or wherever so that we can all know how to experience such a problem should anyone else encounter it.
