# Android Studio
###### Purpose
To provide solutions to problems encountered when dealing with Android Studio.

## Problem 1: Rendering Issues

### Solution 1: Try installing all the SDK tools/libraries related to the API you're working with.

### Solution 2: Try changing the API level.

### Solution 3: Run the application, if your virtual/physical device has no problem rendering it, you're still ok. This isn't an issue for you unless you're working on the UI.

