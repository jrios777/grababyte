package edu.uw.cs.meetagenius;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * This activity displays the full information of the current meal plan.
 * It allows joining and leaving a meal plan.
 *
 * @author Thao Nguyen Dang & Jessica Li
 */
public class MealPlanInfoActivity extends Activity {

    /** The current meal plan */
    MealPlan currentml;
    /** the user id */
    private Long userId;

    /*
     * Called when the activity is starting. Sets up for the view and the text fields
     * to be displayed for the current meal plan being displayed.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down then this Bundle contains the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_meal_plan_info);

        Intent intent = getIntent();
        // get the meal plan info that user clicks to, to display the full information
        Log.d("MPIActivity", "Intent has extras " + intent.getExtras().toString());
        currentml = intent.getParcelableExtra("MealPlan");


        // Up button to go back to previous page [i.e. back button]
        getActionBar().setDisplayHomeAsUpEnabled(true);

        // prepare for the fields to be displayed on description page
        TextView host = byID(R.id.inputHost);

        //TextView host = (TextView) findViewById(R.id.inputHost);
        TextView foodmood = (TextView) findViewById(R.id.inputFoodMood);
        TextView date = (TextView) findViewById(R.id.inputDate);
        TextView time = (TextView) findViewById(R.id.inputTime);
        TextView location = (TextView)findViewById(R.id.inputLocation);
        TextView description = (TextView)findViewById(R.id.inputDescription);

        // set those fields to display the proper values
        host.setText((currentml.getHost().trim().isEmpty()) ? "Unknown" : currentml.getHost().trim());
        foodmood.setText(currentml.getFoodMood().trim());
        Calendar cal = currentml.getDateAndTime();
        SimpleDateFormat sdf = new SimpleDateFormat("EEE, MMM d, yyyy");
        date.setText(sdf.format(cal.getTime()));
        sdf = new SimpleDateFormat("h:mm a");
        time.setText(sdf.format(cal.getTime()));

        location.setText(currentml.getMeetingLocation().trim());
        description.setText(currentml.getDescription().trim());

        // when user clicks on host name, dialog pops up with the host information
        host.setOnClickListener(v -> new GetHostAccountTask().execute(currentml.getHostID()));

        new LoadAttendeesTask().execute();
    }

    private <T extends View> T byID(int id) {
        return (T) findViewById(id);
    }

    /**
     * Given an account, returns an array of the account information
     * in the format [email, gender, school year, major]
     * @param account the account to retrieve the information
     * @return String array of account information
     */
    private String[] getAccountInfo(Account account) {
        String[] info = new String[4];
        info[0] = "Email: " + account.getEmail();
        if (account.getGender() == 'M') {
            info[1] = "Gender: Male";
        } else if (account.getGender() == 'F') {
            info[1] = "Gender: Female";
        } else {
            info[1] = "Gender: Unknown";
        }
        info[2] = "School Year: " + account.getSchoolYear().getRepString();
        String major = account.getMajor();
        if (major == null || major.length() == 0) {
            info[3] = "Major: Unknown";
        } else {
            info[3] = "Major: " + major;
        }
        return info;
    }

    /**
     * Concatenates each element in the array with the given delimiter
     * to a string
     * @param arr
     * @return the string
     */
    private String arrayToString(String[] arr, String delimiter) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < arr.length; i++) {
            res.append(arr[i] + delimiter);
        }
        return res.toString();
    }

    /* This method is called when an item in options is selected. It returns the user to
     * home when the home button is selected */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Listen to the behavior of user.
     * Navigate to MyMealPlanFragment page after success execute action
     * @param v The view that has been clicked.
     */
    public void onClick(View v){
        new InteractWithMealPlan().execute(v.getId());
    }

    /**
     * Updates the meal plan information in the database by adding the user to the meal plan
     * or removing the user from the meal plan in the background
     */
    public class InteractWithMealPlan extends AsyncTask<Integer, Void, Boolean> {

        /* Adds or removes users from a meal plan in the background. Returns a boolean
         * result for whether or not the change in meal plan was successful.
         */
        @Override
        protected Boolean doInBackground(Integer...params) {
            return (params[0] == R.id.Bjoin) ?
                    MealPlanManager.attendPlan(userId, currentml.getPlanID()) :
                    MealPlanManager.attendPlan(userId, currentml.getPlanID());
        }

        /* This method is called after trying to add or remove a user from a meal plan.
         * On success, will display a success message and return the user home. If the
         * server failed to process the request, will display a message that says retry.
         */
        @Override
        protected void onPostExecute(Boolean result) {
            Context displayMessage = getApplicationContext();
            if(result) {
                Toast.makeText(displayMessage, "Success", Toast.LENGTH_SHORT).show();
                Intent homePage = new Intent(MealPlanInfoActivity.this, MainActivity.class);
                int tab = getIntent().getIntExtra("From", 0);
                homePage.putExtra("tab", tab);
                startActivity(homePage);
            } else {
                Toast.makeText(displayMessage, "Retry", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Load the attendees list of the meal plan
     * let user leave meal plan if they are the host or attendee
     * otherwise let user join meal plan
     */
    public class LoadAttendeesTask extends AsyncTask<Void, Void, String> {

        boolean userInPlan;
        private TextView attendeeList; // TextView that displays the list of attendees

        /* Loads the attendee list in the background and returns a list of names
         * of the people attending a meal plan.
         */
        @Override
        protected String doInBackground(Void...params) {
            final List<Account> attendees = new ArrayList<>();
            attendeeList = (TextView) findViewById(R.id.attendees);
            userInPlan = currentml.getHostID() == AccountManager.getUserId() ||
                    MealPlanManager.getAttendees(AccountManager.getUserId(), currentml.getPlanID(), attendees) == 1;
            attendeeList.setOnClickListener( v -> {
                    Set<Long> attendeeIds = new HashSet<Long>(); // list of attendee IDs
                    for (Account a : attendees)
                        attendeeIds.add(a.getId());
                    new GetAttendeeAccountTask().execute(attendeeIds);
            });
            Log.d("JoinMealPlanActivity", attendees.toString());
            String names = "";
            // get the names of attendees,
            // if attendee doesn't input any username,
            // the name displayed in attendee is unknown
            if (attendees.size() > 0) {
                for (Account a : attendees) {
                    String name = a.getName();
                    if (name.length() == 0)
                        name = "unknown";
                    names += name;
                    names += "\n";
                }
                names = names.substring(0, names.length() - 1);
            }
            return names;
        }

        /* This method is called after trying to load the attendee lists. It accepts a string
         * containing the names of the attendees and displays the names to the user. If the
         * user is in the meal plan, a button for leaving the meal plan is displayed. Otherwise,
         * a button for joining the meal plan is displayed.
         */
        @Override
        protected void onPostExecute(String names) {
            // set up the attendee list of that meal plan
            attendeeList.setText(names);
            userId = AccountManager.getUserId();
            Button button = (Button) findViewById(userInPlan ? R.id.BremoveFromPlan : R.id.Bjoin);

            // if the user is the host or attendee, they can leave the plan, else they can join
            LinearLayout.LayoutParams params =
                    new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT,
                                                  LinearLayout.LayoutParams.WRAP_CONTENT);
            int size = (int) Math.floor(16 * getResources().getDisplayMetrics().density);
            params.setMargins(size, size, size, size);
            button.setVisibility(View.VISIBLE);
            button.setLayoutParams(params);
        }
    }

    /**
     * Gets the Host Account of the given Account ID and displays information about the
     * account in a Dialog
     */
    public class GetHostAccountTask extends AsyncTask<Long, Void, Void> {

        private Account account; // account that matches the given ID

        /*
         * Gets the User Account given the Account ID from AccountManager
         */
        @Override
        protected Void doInBackground(Long...params) {
            account = AccountManager.getUserById(params[0]);
            return null;
        }

        /*
         * If result is true, then displays a dialog of the account's information
         */
        @Override
        protected void onPostExecute(Void result) {
            if (account != null) {
                new AlertDialog.Builder(MealPlanInfoActivity.this)
                        .setTitle(currentml.getHost().trim())
                        .setItems(getAccountInfo(account), (d, w) ->  {
                            // if the user clicks on Email, then open up email
                            if (w == 0) sendEmail("GrabAByte: Joining your meal plan", account, null);
                        })
                        .setNegativeButton("Cancel", (d, which) -> d.dismiss())
                        .create().show();
            }
        }
    }

    /**
     * Gets the Attendee Accounts of the given Account ID and displays information about the
     * account in a Dialog
     */
    public class GetAttendeeAccountTask extends AsyncTask<Set<Long>, Void, String[]> {

        List<Account> attendees;

        /*
         * Gets the User Account given the Account ID from AccountManager
         */
        @Override
        protected String[] doInBackground(Set<Long>... params) {
            Set<Long> attendeeIds = params[0]; // list of attendee IDs
            String[] attendeeInfo = new String[attendeeIds.size()];
            attendees = new ArrayList<>();
            // for each attendee, get the Account of that attendee from its ID, then
            // add the account info attendeeInfo
            int i = 0;
            for (Long id: attendeeIds) {
                Account currAccount = AccountManager.getUserById(id);
                if (currAccount != null) {
                    attendees.add(currAccount);
                    String[] currInfo = getAccountInfo(currAccount);
                    attendeeInfo[i] = currAccount.getName() + "\n" + arrayToString(currInfo, "\n");
                    i++;
                }
            }
            return attendeeInfo;
        }

        @Override
        protected void onPostExecute(final String[] attendeeInfo) {
            String subj = "GrabAByte meal plan";
            new AlertDialog.Builder(MealPlanInfoActivity.this)
                    .setTitle("Attendee(s) Information")
                    .setItems(attendeeInfo, (d, which) -> sendEmail(subj, attendees.get(which), null))
                    .setNeutralButton("Email all attendees", (d, which) -> sendEmail(subj, attendees.get(0), attendees))
                    .setNegativeButton("Cancel", (d, which) -> d.dismiss())
                    .create().show();
        }
    }

    /**
     * Sends an email out with the given subject to the 'to' person in attendees list
     * @param subj the subject
     * @param to the main recipient of the email
     * @param attendees the list of attendees
     */
    private void sendEmail(String subj, Account to, List<Account> attendees) {
            Intent emailIntent = new Intent(Intent.ACTION_SEND);
            emailIntent.setType("plain/text");
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, subj);
            emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{to.getEmail()});
            if (attendees != null && attendees.size() > 0) {
                String[] cc = new String[attendees.size() - 1];
                for (int i = 1; i < attendees.size(); i++)
                    cc[i - 1] = attendees.get(i).getEmail();
                emailIntent.putExtra(Intent.EXTRA_CC, cc);
            }
            startActivity(emailIntent);
    }

}
