package edu.uw.cs.meetagenius;

import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TimePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

/**
 *
 * This activity creates a meal plan.
 *
 * @author  Thao Nguyen Dand &
 *
 */
public class CreateMealPlanActivity extends Activity {

    /** A date picker dialog for picking dates */
    private DatePickerDialog datePickerDialog;
    /** A date formatter for formatting dates */
    private SimpleDateFormat dateFormatter;
    /** A time picker dialog for picking time */
    private TimePickerDialog timePickerDialog;
    /** The time of the meal plan */
    private int mHour, mMinute;
    private boolean datePicked;
    private boolean timePicked;

    /* This method is called when the activity is created. It initializes the
     * view and creates listeners that respond to user input for buttons and
     * text fields  */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_create_meal_plan);


        EditText dateText = (EditText) findViewById(R.id.datePicker);
        dateFormatter = new SimpleDateFormat("EEE, MMMM dd, yyyy", Locale.US);
        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // disable past dates: set the minimum date to be the current date
                final Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
                c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
                c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
                c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
                DatePicker datePicker = datePickerDialog.getDatePicker();
                datePicker.updateDate(c.get(Calendar.YEAR) - 1900, c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                datePicker.setMinDate(c.getTimeInMillis());
                datePickerDialog.show();

                datePicked = true;
            }
        });
        dateText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    datePickerDialog.show();
                } else {
                    datePickerDialog.dismiss();
                }

                datePicked = true;
            }

        });

        EditText timeText = (EditText) findViewById(R.id.timePicker);
        timeText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                timePickerDialog.show();

                timePicked = true;
            }
        });
        timeText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    timePickerDialog.show();
                } else {
                    timePickerDialog.dismiss();
                }
                timePicked = true;
            }
        });

        setDateTimeField(dateText, timeText);

        // Specify that the Home button should show an "Up" caret, indicating that touching the
        // button will take the user one step up in the application's hierarchy. [i.e. back button]
        getActionBar().setDisplayHomeAsUpEnabled(true);
        Button createButton = (Button) findViewById(R.id.Bcreate);
        createButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditText foodMoodText = (EditText)findViewById(R.id.inputFoodMood);
                String foodmood = foodMoodText.getText().toString().trim();
                EditText locationText = (EditText)findViewById(R.id.inputLocation);
                String location = locationText.getText().toString().trim();

                if (datePicked && timePicked && foodmood.length() > 0 && location.length() > 0) {
                    new createPlanTask().execute();
                } else {
                    Toast.makeText(getBaseContext(), "All fields required, except Description.",
                            Toast.LENGTH_LONG).show();
                }
            }
        });
    }


    /**
     * Sets the date of the EditText dateText to the date of the DatePickerDialog
     * @param dateText
     */
    private void setDateTimeField(final EditText dateText, final EditText timeText) {

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(year, monthOfYear, dayOfMonth);
                dateText.setText(dateFormatter.format(newDate.getTime()));
            }

        },newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(this, new TimePickerDialog.OnTimeSetListener() {
            @Override
            public void onTimeSet(TimePicker view, int hourOfDay, int minute) {
                String hour = hourString(hourOfDay);
                String min = minuteString(minute);
                String amPm = amPm(hourOfDay);
                timeText.setText(hour + ":" + min + " " + amPm);
                mHour = hourOfDay;
                mMinute = minute;
            }
        }, mHour, mMinute, false);
    }

    /**
     * Returns a String representation for the given hour
     * @param hour
     * @return String representation of hour in standard time
     */
    private String hourString(int hour) {
        String result;
        if (hour <= 12) {
            result = hour + "";
        } else {
            result = hour - 12 + "";
        }
        return result;
    }

    /**
     * Returns a String representation for the given minute
     * @param min
     * @return String representation for min
     */
    private String minuteString(int min) {
        String result;
        if (min < 10) {
            result = "0" + min;
        } else {
            result = min + "";
        }
        return result;
    }

    /**
     * Returns the time of day (AM or PM) determined by the given hour
     * in military time
     * @param hour
     * @return String AM if hour <= 12 or PM otherwise
     */
    private String amPm(int hour) {
        if (hour > 12) {
            return "PM";
        } else {
            return "AM";
        }
    }


    /**
     * A class that creates meal plans in the background. It refreshes the page
     * if on successful creation of a meal plan or displays an error message if
     * the meal plan failed to be created
     */
    public class createPlanTask  extends AsyncTask<Void, Void, Long> {

        /* Attempts to create a meal plan in the background. If meal plan creation is
         * successful, returns 1. If failed, returns -1.
         */
        @Override
        protected Long doInBackground(Void... params) {
            // extract foodmood, date, time, location, description
            EditText foodMoodText = (EditText)findViewById(R.id.inputFoodMood);
            String foodmood = foodMoodText.getText().toString().trim();

            DatePicker datePicker = datePickerDialog.getDatePicker();
            int year = datePicker.getYear();
            int month = datePicker.getMonth();
            int day = datePicker.getDayOfMonth();

            Calendar cal = Calendar.getInstance();
            cal.set(year, month, day, mHour, mMinute);


            EditText locationText = (EditText)findViewById(R.id.inputLocation);
            String location = locationText.getText().toString().trim();

            EditText descriptionText = (EditText)findViewById(R.id.inputDescription);
            String description = descriptionText.getText().toString().trim();

            String hostname = AccountManager.getUserName();
            long hostId = AccountManager.getUserId();

            return MealPlanManager.addPlan(hostname, hostId, cal, foodmood, location, description);
        }

        /* After attempting to create a meal plan in the background. This method will be
         * called. It accepts a long createSuccess, which is -1 if the meal plan failed to
         * be created, 1 if creation was successful. Will redirect the user to my meal plan
         * manager on success and refreshes the page. If failed, will display an error message
         * requesting the user to check their internet connection.
         */
        @Override
        protected void onPostExecute(Long createSuccess) {
            if (createSuccess != -1) {
                // success, auto redirect user to my meal plans manager
                // if user click refresh, their page will be updated with new meal plan
                Intent intent = new Intent(CreateMealPlanActivity.this, MainActivity.class);
                intent.putExtra("tab", 1);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
            } else {
                // add plan fail
                Context displayMessage = getApplicationContext();
                Toast.makeText(displayMessage, "Failed to create Meal Plan. Please check connection",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    /* This method is called when an item in options is selected. It returns the user to
     * home when the home button is selected */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }
}
