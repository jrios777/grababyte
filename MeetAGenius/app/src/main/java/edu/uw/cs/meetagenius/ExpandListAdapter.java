package edu.uw.cs.meetagenius;

/**
 * Represents an adapter for an expandable list.
 *
 * Created by jessicali on 4/23/15.
 */

/*
 * Representation invariant:
 * mGroups != null && inf != null
 */
import java.lang.Object;import java.util.ArrayList;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

public class ExpandListAdapter extends BaseExpandableListAdapter {

    private ArrayList<ExpandListGroup> mGroups;
    private final LayoutInflater inf;

    /**
     * Constructs a new ExpandListAdapter
     * @param activity The activity that the adapter acts on
     * @param groups The list of groups that the adapter accesses
     */
    public ExpandListAdapter(Activity activity, ArrayList<ExpandListGroup> groups) {
        this.mGroups = groups;
        inf = LayoutInflater.from(activity);
    }

    /* Given a position, returns the coreesponding expand list group object for it */
    @Override
    public Object getGroup(int groupPosition) {
        return mGroups.get(groupPosition);
    }

    /* Returns total the number of expand list groups objects  */
    @Override
    public int getGroupCount() {
        return mGroups.size();
    }

    /* Given a int groupPosition, returns it as a long */
    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    /* Given a position of a meal plan, sets up and returns the display for the position
     * of the meal plan. */
    @Override
    public View getGroupView(int groupPosition, boolean isLastChild, View view,
                             ViewGroup parent) {
        ExpandListGroup group = (ExpandListGroup) getGroup(groupPosition);
        if (view == null) {
            view = inf.inflate(R.layout.expandlist_group_item, null);
        }
        TextView tv1 = (TextView) view.findViewById(R.id.tvGroupHost);
        tv1.setText(group.getHost());

        TextView tv2 = (TextView) view.findViewById(R.id.tvGroupDate);
        tv2.setText(group.getDate());

        TextView tv3 = (TextView) view.findViewById(R.id.tvGroupTime);
        tv3.setText(group.getTime());

        TextView tv4 = (TextView) view.findViewById(R.id.tvGroupFood);
        tv4.setText(group.getFood());

        return view;
    }

    /* Assumes there is a stable id and returns true. */
    @Override
    public boolean hasStableIds() {
        return true;
    }

    /* Assumes the item at the specified position is selectable and returns true. */
    @Override
    public boolean isChildSelectable(int arg0, int arg1) {
        return true;
    }

    /* We are not using children, so we wiill return null. */
    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild,
                             View view, ViewGroup parent) {
        return null;
    }

    /* Returns the childPosition in long format */
    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    /* Returns the default meeting location string (at cse atrium) */
    @Override
    public Object getChild(int groupPosition, int childPosition) {
        // TODO Auto-generated method stub
        return "Location: Meet at Atrium";
    }

    /* Returns zero, since we are not using children. */
    @Override
    public int getChildrenCount(int groupPosition) {
        return 0;
    }
}