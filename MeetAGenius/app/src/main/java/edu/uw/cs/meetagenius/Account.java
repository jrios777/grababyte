package edu.uw.cs.meetagenius;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

/**
 * Account represents a single user that holds certain properties/information about that user.
 *      Each account user must have a valid email and id. Other properties are optional.
 *
 * Rep Invariant:
 *  email != NULL &&
 *  for each valid user id >= 0 (Not checked in latest commit)
 *
 * Abstract Function:
 *  name = the name of the user
 *  email = the user's email/login
 *  id = the user's account assigned id number
 *  schoolYear = the year or educational status of account user
 *  gender = gender of the user
 *
 * @author Michelle Cho
 */
@AllArgsConstructor
@RequiredArgsConstructor
@Getter // for every field!
@Setter // for every non-final field!
public class Account implements Table {
    // static constants used between AccountManager and DBManager
    public static final String KEY_ID = "id";
    public static final String KEY_EMAIL = "email";
    public static final String KEY_NAME = "name";
    public static final String KEY_YEAR = "year";
    public static final String KEY_GENDER = "gender";
    public static final String KEY_MAJOR = "major";

    // instance fields for an Account object
    @NonNull private final Long id;  // unique identifier for database
    @NonNull private String email;
    private String name;
    private Year schoolYear = Year.UNKNOWN; // Default value
    private char gender = 'U'; // Default value
    private String major;

    /**
     * Year represents the user's school year
     */
    public enum Year {
        UNKNOWN(0, "Unknown"),
        FRESHMAN(1, "Freshman"),
        SOPHOMORE(2, "Sophomore"),
        JUNIOR(3, "Junior"),
        SENIOR(4, "Senior"),
        GRAD(5, "Graduate"),
        FACULTY(6, "Faculty"),
        UNDERGRAD(7, "Undergrad");

        private final int repNumber;
        private final String repString;

        Year(int repNumber, String repString) {
            this.repNumber = repNumber;
            this.repString = repString;
        }

        public int getRepNumber() {
            return repNumber;
        }

        public String getRepString() { return repString; }

        public static Year toYear(int year) {
            for (Year y: Year.values()) {
                if (y.repNumber == year)
                    return y;
            }
            throw new IllegalArgumentException("Bad arg: " + year);
        }
    }
}
