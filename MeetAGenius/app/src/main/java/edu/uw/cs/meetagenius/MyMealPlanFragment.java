package edu.uw.cs.meetagenius;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Thao Nguyen Dang &
 * This is Meal Plan Management page
 * Display 2 lists of meal plan: hosting meal plans and attending meal plan
 * the meal plan are displayed in sorted order from closet date to further day
 * Allow user to expand the description of meal plan by clicking to one item
 */
public class MyMealPlanFragment extends android.support.v4.app.Fragment implements ExpandableListView.OnGroupClickListener {

    /** A key used for tab navigation when handling intents */
    public static final String ARG_SECTION_NUMBER = "section_number";
    /** A list of meal plans the user is hosting */
    private List<MealPlan> hostingList;
    /** A list of meal plans the user is attending */
    private List<MealPlan> attendingList;
    /** A expandable view of meal plans the user is hosting */
    private ExpandableListView attendingListView;
    /** A expandable view of meal plans the user is hosting */
    private ExpandableListView hostingListView;

    /*
     * Called when the activity is starting. Calls the default onCreate.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down then this Bundle contains the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    /*
     * Called to have the fragment instantiate its user interface view
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous
     *                           saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_my_meal_plan, container, false);
        Button addMealPlan = (Button) rootView.findViewById(R.id.BaddPlan);
        addMealPlan.setOnClickListener(v -> {
            // if there is internet connection, navigate user to add meal plan page
            // otherwise display an error message
            if (NetworkConnection.checkConnection(getActivity())) {
                Intent i = new Intent(getActivity(), CreateMealPlanActivity.class);
                startActivity(i);
            } else {
                Toast.makeText(getActivity(), "Check internet connection.", Toast.LENGTH_SHORT).show();
            }
        });
        return rootView;
    }

    /*
     * Called immediately after onCreateView(LayoutInflater, ViewGroup, Bundle) has returned,
     * but before any saved state has been restored in to the view.
     * Allow user to move between pages but still get update on the app's state
     */
    @Override
    public void onResume() {
        super.onResume();
        View view = getView();
        attendingListView = (ExpandableListView) view.findViewById(R.id.attending_list);
        hostingListView = (ExpandableListView) view.findViewById(R.id.hosting_list);
        hostingListView.setOnGroupClickListener(this);
        attendingListView.setOnGroupClickListener(this);
        Log.d("Tag", "set the listener");
        // check if there is internet connection:
        // if there is, continue to load the meal plans
        // otherwise display an error message
        if (NetworkConnection.checkConnection(getActivity())) {
            new LoadUserPlansTask().execute();
        } else {
            Toast.makeText(getActivity(), "Check internet connection.", Toast.LENGTH_SHORT).show();
        }
    }

    /*
     * Invoked when a group in the expandable list has been clicked.
     * Starts the JoinMealPlanActivity for the meal plan that is clicked.
     *
     * @param parent The ExpandableListConnector where the click happened
     * @param view The view within the expandable list that was clicked
     * @param position The group position that was clicked
     * @param id The row id of the group that was clicked
     * @return true if the click was handled, false on failure
     */
    @Override
    public boolean onGroupClick(ExpandableListView parent, View view, int position, long id) {
        Intent i = new Intent(getActivity(), MealPlanInfoActivity.class);
        i.putExtra("From", 1);
        Log.d("TAG", "clicked something");
        MealPlan selected = (hostingListView.getId() == parent.getId()) ?
                hostingList.get(position) : attendingList.get(position);

        i.putExtra("MealPlan", selected);
        Log.d("TAG", "starting activity");
        startActivity(i);
        return true;
    }

    /**
     * Load the list of hosting meal plans and a list of attending meal plans
     * by giving current user id
     */
    public class LoadUserPlansTask extends AsyncTask<Void, Void, Boolean> {
        private ArrayList<ExpandListGroup> hostingListItems = new ArrayList<>();
        private ArrayList<ExpandListGroup> attendingListItems = new ArrayList<>();

        /* Loads the list of hosting meal plans and attending meal plans in the
         * background */
        @Override
        protected Boolean doInBackground(Void... params) {
            long userId = AccountManager.getUserId();
            attendingList = MealPlanManager.getAttendingList(userId);
            hostingList = MealPlanManager.getHostingList(userId);

            if (attendingList == null || hostingList == null)
                return false;

            // set up the attending list to the display
            for(MealPlan mp : attendingList)
                attendingListItems.add(new ExpandListGroup(mp));

            // set up the hosting list to the display
            for(MealPlan mp : hostingList)
                hostingListItems.add(new ExpandListGroup(mp));

            return true;
        }

        /* Called after attempting to load the list of meal plans the user is attending/hosting. If
         * successful, then will update the view to display the new information. */
        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            // set up the 2 lists of meal plans to display
            ExpandListAdapter expAdapter = new ExpandListAdapter(getActivity(), attendingListItems);
            ExpandListAdapter expAdapter2 = new ExpandListAdapter(getActivity(), hostingListItems);
            attendingListView.setAdapter(expAdapter);
            hostingListView.setAdapter(expAdapter2);
        }
    }
}
