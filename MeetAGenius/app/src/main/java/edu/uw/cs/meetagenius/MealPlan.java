package edu.uw.cs.meetagenius;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NonNull;
import lombok.ToString;

/**
 * MealPlan represents a single MealPlan created by an account user. Implements Parcelable and Table.
 *  Each MealPlan has a host, planID, hostID, dataAndTime, foodMood, meetingLocation.
 *  Description of MealPlan is optional
 *
 * Representation Invariant:
 *  host != null
 *  planID >= 0 (Not checked in latest commit)
 *  hostID >= 0 (Not checked in latest commit)
 *  dataAndTime != null
 *  foodMood != null
 *  meetingLocation != null
 *
 * Abstract Function:
 * host = the name of account user that created the MealPlan
 * planID = the assigned planID of MealPlan
 * hostID = the assigned account user ID
 * dataAndTime = the set date and time of the desired MealPlan that the host selects
 * foodMood = the type of food the host desires
 * meetingLocation = the meetingLocation that the host specifies
 * description = a description of the MealPlan written by the host
 *
 * @author Michelle Cho
 */
@AllArgsConstructor
@Getter
@ToString
public class MealPlan implements Parcelable, Table {
    public static final String KEY_ID = "id";
    public static final String KEY_HOST_ID = "hostid";
    public static final String KEY_TIME = "time";
    public static final String KEY_FOODMOOD = "foodmood";
    public static final String KEY_LOCATION = "location";
    public static final String KEY_DESCRIPTION = "description";

    /** Not a table attribute*/
    public static final String KEY_HOSTNAME = "hostname";

    /** formatted date and time */
    private static final SimpleDateFormat sdf = new SimpleDateFormat("EEE MMM dd HH:mm:ss z yyyy", Locale.US);

    @NonNull private String host;
    @NonNull private Long planID;
    @NonNull private Long hostID;
    @NonNull private Calendar dateAndTime;
    @NonNull private String foodMood;
    private String meetingLocation;
    private String description;

    /** The following are methods are for Parcelable */

    /**
     * describes the kinds of special objects contained in this Parcelable's marshalled representation
     * @return bitmask indicating set of special object types marshalled by the Parcelable
     */
    public int describeContents() {
        return 0;
    }

    /**
     * Writes and stores all necessary fields of MealPlan into a Parcel
     * @param out The Parcel in which object should be written to
     * @param flags how object should be written
     */
    public void writeToParcel(Parcel out, int flags) {
        out.writeString(host);
        out.writeLong(planID);
        out.writeLong(hostID);
        out.writeString(sdf.format(dateAndTime.getTime()));
        out.writeString(foodMood);
        out.writeString(meetingLocation);
        out.writeString(description);
    }

    /**
     * Implementation for abstract methods in Parcelable.
     */
    public static final Parcelable.Creator<MealPlan> CREATOR
            = new Parcelable.Creator<MealPlan>() {
        public MealPlan createFromParcel(Parcel in) {
            return new MealPlan(in);
        }

        public MealPlan[] newArray(int size) {
            return new MealPlan[size];
        }
    };

    /**
     * Constructs and initializes the state of this MealPlan with all required information.
     * @param in Parcel that holds all the fields of this MealPlan object.
     */
    private MealPlan(Parcel in) {
        host = in.readString();
        planID = in.readLong();
        hostID = in.readLong();
        try {
            dateAndTime = Calendar.getInstance();
            dateAndTime.setTime(sdf.parse(in.readString()));
        } catch (Exception e) {
            Log.e("MealPlan", e.getMessage());
        }
        foodMood = in.readString();
        meetingLocation = in.readString();
        description = in.readString();
    }
}
