package edu.uw.cs.meetagenius;

import android.app.ActionBar;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.TimePickerDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.text.format.Time;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TimePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Locale;


/**
 * Activity for filtering meal plan search results
 *
 * Created by jessicali on 4/23/15.
 */

public class FilterActivity extends Activity {

    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat dateFormatter; // to format the date on the EditText
    private TimePickerDialog timePickerDialog;
    private int mHour, mMinute;

    /**
     * Called when the activity is starting.
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     * down then this Bundle contains the data it most recently supplied in onSaveInstanceState(Bundle).
     * Otherwise it is null.
     */
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_filter_mealplans);

        // Set up action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home button should show an "Up" caret, indicating that touching the
        // button will take the user one step up in the application's hierarchy.
        actionBar.setDisplayHomeAsUpEnabled(true);

        // set up Date and Time EditText Listeners to open up the DatePickerDialog
        // and TimePickerDialog when pressed
        EditText dateText = (EditText) findViewById(R.id.filter_date);
        dateFormatter = new SimpleDateFormat("EEE, MMMM dd, yyyy", Locale.US);
        dateText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // disable past dates: set the minimum date to be the current date
                final Calendar c = Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
                c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
                c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
                c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
                DatePicker datePicker = datePickerDialog.getDatePicker();
                datePicker.updateDate(c.get(Calendar.YEAR) - 1900, c.get(Calendar.MONTH), c.get(Calendar.DAY_OF_MONTH));
                datePicker.setMinDate(c.getTimeInMillis());
                datePickerDialog.show();
            }
        });
        dateText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) datePickerDialog.show();
            else datePickerDialog.hide();
        });

        EditText timeText = (EditText) findViewById(R.id.filter_time);
        timeText.setOnFocusChangeListener((v, hasFocus) -> {
            if (hasFocus) timePickerDialog.show();
            else timePickerDialog.hide();
        });
        timeText.setOnClickListener((v) -> timePickerDialog.show());

        setDateTimeField(dateText, timeText);

        Button editButton = (Button) findViewById(R.id.filter_search_button);
        editButton.setOnClickListener(v -> {
            // filter button is pressed: get filtered results from database
            // and send user back to home page

            // get the filtered information from the widgets
            EditText hostText = (EditText) findViewById(R.id.filter_host);
            String host = hostText.getText().toString().trim();
            // set host to null if no preference
            if (host.isEmpty()) {
                host = null;
            }

            EditText foodMoodText = (EditText) findViewById(R.id.filter_food_mood);
            String foodMood = foodMoodText.getText().toString().trim();
            // set foodMood to null if no preference
            if (foodMood.isEmpty()) {
                foodMood = null;
            }

            Spinner genderSpinner = (Spinner) findViewById(R.id.filter_gender);
            String gender = genderSpinner.getSelectedItem().toString();
            Character genderChar = 'M';

            if (gender.equalsIgnoreCase("Female")) {
                genderChar = 'F';
            } else if (gender.equalsIgnoreCase("No preference")) {
                // set gender to null if no preference
                genderChar = null;
            }

            Spinner yearSpinner = (Spinner) findViewById(R.id.filter_year);
            int yearPosition = yearSpinner.getSelectedItemPosition();
            Account.Year schoolYear = null;
            if (yearPosition != 0) {
                schoolYear = Account.Year.toYear(yearPosition);
            }

            DatePicker datePicker = datePickerDialog.getDatePicker();
            int year = datePicker.getYear();
            int month = datePicker.getMonth();
            int day = datePicker.getDayOfMonth();

            Calendar cal = Calendar.getInstance();
            cal.set(year, month, day, mHour, mMinute);

            // send query information to Home Fragment
            // send user back to home tab
            Intent intent = new Intent(FilterActivity.this, MainActivity.class);
            intent.putExtra(Account.KEY_GENDER, genderChar);
            intent.putExtra(MealPlan.KEY_HOSTNAME, host);
            intent.putExtra(MealPlan.KEY_TIME, cal);
            intent.putExtra(MealPlan.KEY_FOODMOOD, foodMood);
            intent.putExtra(Account.KEY_YEAR, schoolYear);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
        });
    }

    /**
     * Sets the date and time of the EditTexts to the date of the DatePickerDialog and the time
     * of the TimePickerDialog
     * @param dateText
     */
    private void setDateTimeField(final EditText dateText, final EditText timeText) {

        Calendar newCalendar = Calendar.getInstance();
        datePickerDialog = new DatePickerDialog(this,
                (v, yr, mon, day) ->
                        dateText.setText(dateFormatter.format(new GregorianCalendar(yr, mon, day))),
                newCalendar.get(Calendar.YEAR),
                newCalendar.get(Calendar.MONTH),
                newCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);
        timePickerDialog = new TimePickerDialog(this, (view, hr, min) -> {
            timeText.setText(hourString(mHour = hr) +
                    ":" + minuteString(mMinute = min) +
                    " " + amPm(hr));
        }, mHour, mMinute, false);
    }

    /**
     * Returns a String representation for the given hour
     * @param hour
     * @return String representation of hour in standard time
     */
    private String hourString(int hour) {
        return (hour <= 12) ? hour + "" : hour - 12 + "";
    }

    /**
     * Returns a String representation for the given minute
     * @param min
     * @return String representation for min
     */
    private String minuteString(int min) {
        return (min < 10) ? "0" + min : min + "";
    }

    /**
     * Returns the time of day (AM or PM) determined by the given hour
     * in military time
     * @param hour
     * @return String AM if hour < 12 or PM otherwise
     */
    private String amPm(int hour) {
        return (hour >= 12) ? "PM" : "AM";
    }

    /**
     * This hook is called whenever an item in your options menu is selected.
     * @param item The menu item that was selected.
     * @return boolean false to allow normal menu processing to proceed, true to consume it here
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                // This is called when the Home (Up) button is pressed in the action bar.
                // Create a simple intent that starts the hierarchical parent activity and
                // use NavUtils in the Support Package to ensure proper handling of Up.
                Intent upIntent = new Intent(this, MainActivity.class);
                if (NavUtils.shouldUpRecreateTask(this, upIntent)) {
                    // This activity is not part of the application's task, so create a new task
                    // with a synthesized back stack.
                    TaskStackBuilder.from(this)
                            // If there are ancestor activities, they should be added here.
                            .addNextIntent(upIntent)
                            .startActivities();
                    finish();
                } else {
                    // This activity is part of the application's task, so simply
                    // navigate up to the hierarchical parent activity.
                    NavUtils.navigateUpTo(this, upIntent);
                }
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
