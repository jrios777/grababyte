package edu.uw.cs.meetagenius;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;

import java.util.Calendar;

/**
 * This activity allow user log in to the app.
 *
 * @author  Thao Nguyen Dang &
 */
public class LogInActivity extends Activity implements View.OnClickListener, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {

    /** Request code used to invoke sign in user interactions. */
    private static final int RC_SIGN_IN = 0;

    /** Client used to interact with Google APIs. */
    private static GoogleApiClient mGoogleApiClient;


    /**
     * True if the sign-in button was clicked.  When true, we know to resolve all
     * issues preventing sign-in without waiting.
     */
    private boolean mSignInClicked;

    /**
     * True if we are in the process of resolving a ConnectionResult
     */
    private boolean mIntentInProgress;

    /**A SignInButton for Google+ Login */
    private SignInButton btnLoginGoogle;

    /*
     * Called when the activity is starting. Creates a GoogleAPIClient, sets content view,
     * and adds appropriate listeners to the Google+ login button
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down then this Bundle contains the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_LOGIN)
                .build();

        setContentView(R.layout.activity_log_in);
        // setting the opacity of the background image
        Drawable loginActivityBackground = findViewById(R.id.login_layout).getBackground();
        loginActivityBackground.setAlpha(80);

        btnLoginGoogle = (SignInButton) findViewById(R.id.btnLoginGoogle);
        btnLoginGoogle.setOnClickListener(this);

        // Start IntentService to register this application with GCM.
        Intent intent = new Intent(this, RegistrationIntentService.class);
        startService(intent);

    }

    /**
     * Listen to the behavior of user.
     * Navigate to HomePage after log in button clicked.
     * Navigate to HelpPate if user click Click Here
     * @param v The view that has been clicked.
     */
    public void onClick(View v){

        if (v.getId() == R.id.btnLoginGoogle && !mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            Log.d("clicked", "clicked");
            mGoogleApiClient.connect();

        }
        if (v.getId() == R.id.helpLink) {
            String url = "http://homes.cs.washington.edu/~jrios777/cse403/web/product/help.html";
            Intent launchBrowser = new Intent(Intent.ACTION_VIEW);
            launchBrowser.setData(Uri.parse(url));
            startActivity(launchBrowser);
        }
    }

    /**
     * An class that performs login tasks in the background and leads the user
     * to the home page on successful login.
     */
    public class LoginTask extends AsyncTask<String, Void, Boolean> {

        /*
         * @param params an array of string inputs. If old sign in/sign up
         *               button and text field version, this is empty, in the
         *               Google+ version, we need to give a string containing
         *               the user's email address
         * @return Whether we successfully logged in or not
         *
         * In the case we are using the old sign in / sign up button
         * and text fields, we need to read from the email and password
         * text fields. If we are using the google+ login, we should try create
         * an account with the email extracted from the login.
         */
        @Override
        protected Boolean doInBackground(String... params) {
            // extract the user email
            String email = params[0];
            String name = params[1];
            char gender = params[2].charAt(0);

            if (params.length != 0) {
                email = params[0];
            }

            Log.d("LoginTask", email);

            boolean loginStatus = AccountManager.login(email);
            if (loginStatus && AccountManager.getUserName().equals("")) {
                AccountManager.updateAccount(name, AccountManager.getUserSchoolYear(),
                        gender, AccountManager.getUserMajor());
            }
            return loginStatus;
        }

        /*
         * @param loginSuccess Whether we successfully logged in or not.
         *
         * If we succeeded, we need to send the user to the home page. Otherwise, we need
         * to inform them that their user/password combination is incorrect.
         */
        @Override
        protected void onPostExecute(Boolean loginSuccess) {
            if (loginSuccess) {
                // if it's correct, navigate to Home page.
                Intent homePage = new Intent(LogInActivity.this, MainActivity.class);
                homePage.putExtra(Account.KEY_GENDER, (Character) null);
                homePage.putExtra(MealPlan.KEY_HOSTNAME, (String) null);
                homePage.putExtra(MealPlan.KEY_TIME, (Calendar) null);
                homePage.putExtra(MealPlan.KEY_FOODMOOD, (String) null);
                homePage.putExtra(Account.KEY_YEAR, (Account.Year) null);
                homePage.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                homePage.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(homePage);
            } else {
                // else display pop up message "Invalid user/ password"
                // on login page
                Context displayMessage = getApplicationContext();
                Toast.makeText(displayMessage, "Login failed. Please try again.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /*
     * When this class starts, we need to connect to the Google API Client.
     */
    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    /*
     * When app is stopped, we need to disconnect the GoogleAPIClient.
     */
    @Override
    protected void onStop() {
        super.onStop();

        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    /*
     * @param result The Connection Result
     * When the connection fails, we need to either try again, or stop if they cancel.
     */
    @Override
    public void onConnectionFailed(ConnectionResult result) {
        Log.e("TAG", "Error Code: " + result.getErrorCode());
        Log.e("TAG", result.toString());
        if (!mIntentInProgress) {
            if (mSignInClicked && result.hasResolution()) {
                // The user has already clicked 'sign-in' so we attempt to resolve all
                // errors until the user is signed in, or they cancel.
                try {
                    result.startResolutionForResult(this, RC_SIGN_IN);
                    mIntentInProgress = true;
                } catch (IntentSender.SendIntentException e) {
                    // The intent was canceled before it was sent.  Return to the default
                    // state and attempt to connect to get an updated ConnectionResult.
                    mIntentInProgress = false;
                    mGoogleApiClient.connect();
                }
            }
        }
    }

    /*
     * @param connectionHint
     * When connected, if they have a uw email, we need to sign them in. Otherwise we must
     * inform them to use an email that ends with uw.edu, disconnect them, and allow them to
     * try again.
     */
    @Override
    public void onConnected(Bundle connectionHint) {
        Log.d("Entered on connected", "Entered on connected");
        mSignInClicked = false;
        String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

        // figure out if the user has pressed logout or not...
        SharedPreferences sharedPref = getApplicationContext().
                getSharedPreferences(getString(R.string.has_logged_out), Context.MODE_PRIVATE);
        int logout = sharedPref.getInt(getString(R.string.has_logged_out), 0);

        if (logout == 1) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putInt(getString(R.string.has_logged_out), 0);
            editor.commit();
            mGoogleApiClient.disconnect();
        } else {
            if (!email.endsWith("uw.edu")) {
                // inform the user that they need to add their uw email to their accounts
                Toast.makeText(this, email + " does not end with uw.edu, " +
                        "please add your uw email to your accounts!", Toast.LENGTH_LONG).show();
                // sign out
                if (mGoogleApiClient.isConnected()) {
                    Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
                    mGoogleApiClient.disconnect();
                    mGoogleApiClient.connect();
                }
            } else {
                // extract there user info...
                String name = "";
                String gender = "U";
                if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                    Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                    Person.Name person_name = currentPerson.getName();
                    String first = person_name.getGivenName();
                    String last = person_name.getFamilyName();
                    name = first + " " + last;
                    int person_gender = currentPerson.getGender();
                    if (person_gender == Person.Gender.MALE) {
                        gender = "M";
                    } else if (person_gender == Person.Gender.FEMALE) {
                        gender = "F";
                    } else {
                        gender = "U";
                    }
                }
                new LoginTask().execute(email, name, gender);
            }
        }
    }

    /*
     * @param requestCode The type of activity
     * @param responseCode The result of activity
     * @param intent
     * If we tried to sign in and failed, then we need to set signed in clicked to false and
     * intent in progress to false. We also need to try reconnect the api client.
     */
    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnected()) {
                mGoogleApiClient.reconnect();
            }
        }
    }

    /* When the user tries to logout, a new intent leading back to the original
     * log in activity should be created. When this occurs, we will try to log
     * the user out if the client is connected. */
    @Override
    public void onNewIntent(Intent intent) {
        if (mGoogleApiClient.isConnected()) {
            Log.e("We have tried to logout","We have tried to logout");
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            mGoogleApiClient.disconnect();
            mGoogleApiClient.connect();
        }
    }

    /*
     * @param cause
     * If connection is suspended, we need to try connect them again
     */
    @Override
    public void onConnectionSuspended(int cause)
    {
       mGoogleApiClient.connect();
    }
}
