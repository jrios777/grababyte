package edu.uw.cs.meetagenius;

/**
 * This class does logging according to whether the logging is being done on phone
 * or in the unit tests to std err. It's identical to android.util.Log when CONSOLE = false
 * and just dumps messages to std err when true.
 *
 * Created by jrios777 on 5/22/15.
 */
public class Log {
    /** Whether or not we're writing to standard err in the terminal */
    public static boolean CONSOLE = false;

    public static void d(String tag, String message) {
        if (CONSOLE) stderr(tag + ":" + message);
        else android.util.Log.d(tag, message);
    }

    public static void e(String tag, String message) {
        if (CONSOLE) stderr(tag + ":" + message);
        else android.util.Log.e(tag, message);
    }

    public static void e(String tag, String message, Throwable e) {
        if (CONSOLE) { stderr(tag + ":" + message); e.printStackTrace(); }
        else android.util.Log.e(tag, message, e);
    }

    public static void w(String tag, String message) {
        if (CONSOLE) stderr(tag + ":" + message);
        else android.util.Log.w(tag, message);
    }

    public static void v(String tag, String message) {
        if (CONSOLE) stderr(tag + ":" + message);
        else android.util.Log.v(tag, message);
    }

    public static void i(String tag, String message) {
        if (CONSOLE) stderr(tag + ":" + message);
        else android.util.Log.i(tag, message);
    }

    public static void stderr(String out) {
        System.err.println(out);
    }
}
