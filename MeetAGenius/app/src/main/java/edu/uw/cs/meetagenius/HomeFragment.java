package edu.uw.cs.meetagenius;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

/**
 * Fragment for Home which displays a list of the available meal plans and a filter option
 *
 * Created by jessicali on 5/13/15.
 */
public class HomeFragment extends Fragment implements ExpandableListView.OnGroupClickListener {

    // An expand list adapter for choosing which items to display for a meal plan on Home page
    private ExpandListAdapter expAdapter;
    // A list storing all the meal plans
    private List<MealPlan> mealPlans;
    // An expand list view for showing specific items to display for a meal plan on Home page
    ExpandableListView expandList;

    // The host for a meal plan
    private String host;
    // The gender of a meal plan
    private Character gender;
    // The time of a meal plan
    private Calendar cal;
    // The food mood for a meal plan
    private String foodMood;
    // The school year of the host of the meal plan
    private Account.Year schoolYear;

    /**
     * Called when the activity is starting. This initializes the view to display the
     * list of meal plans.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down then this Bundle contains the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // set up the list of meal plans when app opens
        Intent i = getActivity().getIntent();
        foodMood = i.getStringExtra(MealPlan.KEY_FOODMOOD);
        host = i.getStringExtra(MealPlan.KEY_HOSTNAME);
        gender = (Character) i.getSerializableExtra(Account.KEY_GENDER);
        schoolYear = (Account.Year) i.getSerializableExtra(Account.KEY_YEAR);
        cal = (Calendar) i.getSerializableExtra(MealPlan.KEY_TIME);
    }

    /**
     * Called immediately after onCreateView(LayoutInflater, ViewGroup, Bundle) has returned,
     * but before any saved state has been restored in to the view. This changes the view so
     * that it displays the list of meal plans
     *
     * @param view The View returned by onCreateView(LayoutInflater, ViewGroup, Bundle)
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a
     *                           previous saved state as given here.
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        expandList = (ExpandableListView) view.findViewById(R.id.meal_plan_feed);
        expandList.setOnGroupClickListener(this);
    }

    /**
     * This method is called when the user returns to the home fragment. It refreshes
     * the list of meal plans
     */
     @Override
    public void onResume() {
        super.onResume();
        // if there is internet, load the mean plans; otherwise print a error statement
        if (NetworkConnection.checkConnection(getActivity())) {
            new LoadMealPlansTask().execute();
        } else {
            Toast.makeText(getActivity(), "Check internet connection.", Toast.LENGTH_LONG).show();
        }
    }

    /**
     * Called to have the fragment instantiate its user interface view
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous
     *                           saved state as given here.
     * @return the View for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        // open up homepage
        View rootView = inflater.inflate(R.layout.fragment_home, container, false);
        rootView.findViewById(R.id.filter_button).setOnClickListener((v) ->
                startActivity(new Intent(getActivity(), FilterActivity.class)));
        return rootView;
    }

    /**
     * Invoked when a group in the expandable list has been clicked.
     * Starts the JoinMealPlanActivity for the meal plan that is clicked.
     *
     * @param parent The ExpandableListConnector where the click happened
     * @param view The view within the expandable list that was clicked
     * @param position The group position that was clicked
     * @param id The row id of the group that was clicked
     * @return true if the click was handled, false on failure
     */
    @Override
    public boolean onGroupClick(ExpandableListView parent, View view, int position, long id) {
        Intent i = new Intent(getActivity(), MealPlanInfoActivity.class);
        i.putExtra("MealPlan", mealPlans.get(position));
        i.putExtra("From", 0);
        // start JoinMealPlanActivity
        startActivity(i);
        return true;
    }

    /**
     * Loads the meal plans from the database and displays them as a sorted list
     */
    public class LoadMealPlansTask extends AsyncTask<Void, Void, ArrayList<ExpandListGroup>> {

        /* Loads the meal plans from the database in the background and returns a sorted list
         * of the meal plans */
        @Override
        protected ArrayList<ExpandListGroup> doInBackground(Void... params) {
            ArrayList<ExpandListGroup> list = new ArrayList<ExpandListGroup>();
            // get all the meal plans in the database

            // Is there a filter? Or none?
            mealPlans = (host == null && gender == null && cal == null && foodMood == null && schoolYear == null) ?
                    MealPlanManager.getAllPlans() :
                    MealPlanManager.query(host, gender, schoolYear, cal, foodMood);

            // for each meal plan in the list mealPlans, create a ExpandListGroup
            // for that plan so that it can be displayed
            for(MealPlan mp : mealPlans)
                list.add(new ExpandListGroup(mp));
            return list;
        }

        /**
         * After loading a list of meal plans, this method is called. It accepts an
         * ArrayList of ExpandListGroups (containing meal plans) and displays them
         * to the user.
         * @param expandListGroups
         */
        @Override
        protected void onPostExecute(ArrayList<ExpandListGroup> expandListGroups) {
            super.onPostExecute(expandListGroups);
            expAdapter = new ExpandListAdapter(getActivity(), expandListGroups);
            expandList.setAdapter(expAdapter);
        }
    }

}

