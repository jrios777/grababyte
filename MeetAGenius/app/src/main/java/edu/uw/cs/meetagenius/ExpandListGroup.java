package edu.uw.cs.meetagenius;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;

/**
 * Represents a group for an expandable list
 * The group stores information about a meal plan, including a host, date, time, and food.
 *
 * Created by jessicali on 4/23/15.
 */

/*
Representation invariant:
host != null && date != null && time != null && food != null
 */
@Getter
@Setter
@NoArgsConstructor
public class ExpandListGroup {
    @NonNull private String host = "";
    @NonNull private String date = "";
    @NonNull private String time = "";
    @NonNull private String food = "";

    public ExpandListGroup(MealPlan mp) {
        host = (mp.getHost().length() == 0) ? "Unknown" : mp.getHost();
        food = mp.getFoodMood();
        setDateAndTime(mp.getDateAndTime());
    }

    /**
     * Sets the date and time of this ExpandListGroup to the given Calendar
     * @param cal
     */
    public void setDateAndTime(Calendar cal) {
        SimpleDateFormat sdfDate = new SimpleDateFormat("EEE, MMM dd, yyyy");
        SimpleDateFormat sdfTime = new SimpleDateFormat("h:mm a");
        date = sdfDate.format(cal.getTime());
        time = sdfTime.format(cal.getTime());
    }
}
