package edu.uw.cs.meetagenius;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by MichelleCho on 5/26/15.
 *
 * NetworkConnection is a utility class that will deal with checking the internet connection.
 *
 */
public class NetworkConnection extends Activity {

    /**
     * This checks to see if the given View has connection to the internet
     * @param context the Current View that needs to check for internet connection
     * @return true if the the view is connected to the internet, and false otherwise
     */
    public static boolean checkConnection(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
