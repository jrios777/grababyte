package edu.uw.cs.meetagenius;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;
import java.util.TimeZone;

/**
 * MealPlanManager handles all the interactions between MealPlans and the database.
 * This class is non-instantiable and should be used as a utility class.
 *
 * @author Junhao Zhu
 */
public class MealPlanManager {

    // Calendar formatting
    private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    // Debugging
    private static final String TAG = "MealPlanManager";

    /**
     * Add a new meal plan to the database
     * @param hostname the host's name
     * @param hostId the host's unique id
     * @param dateAndTime meal plan date and time
     * @param foodmood food mood
     * @param location food location
     * @param description user's description (optional)
     * @return an actual plan id if add success
     *         -1 if add fails
     */
    public static long addPlan(String hostname, long hostId, Calendar dateAndTime,
                                  String foodmood, String location, String description) {
        long planID = DBManager.insert(MealPlan.class,
                constructProperties(hostname, hostId, dateAndTime, foodmood, location, description));
        AWS.getAWS(MainActivity.context, null).createPlan(planID);
        return planID;
    }

    /**
     * Private helper function for constructing ContentValue object, used for DBManager
     * @param hostname the host's name
     * @param hostId the host's unique id
     * @param dateAndTime meal plan date and time
     * @param foodmood food mood
     * @param location food location
     * @param description user's description
     * @return a ContentValues object containing all input information
     */
    private static Properties constructProperties(String hostname, long hostId, Calendar dateAndTime,
                                                        String foodmood, String location, String description) {
        Properties newPlan = new Properties();
        // Convert time to GMT zone for database storage
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));

        newPlan.setProperty(MealPlan.KEY_HOSTNAME, hostname);
        newPlan.setProperty(MealPlan.KEY_HOST_ID, Long.toString(hostId));
        newPlan.setProperty(MealPlan.KEY_TIME, sdf.format(dateAndTime.getTime()));
        newPlan.setProperty(MealPlan.KEY_FOODMOOD, foodmood);
        newPlan.setProperty(MealPlan.KEY_LOCATION, location);
        newPlan.setProperty(MealPlan.KEY_DESCRIPTION, description);

        return newPlan;
    }

    /**
     * The user ask to join a meal plan, process to become an attendee for the plan
     * @param userId the user's id
     * @param planId the plan's id
     * @return true if the user is successfully added to the plan's attendee list
     *         false otherwise, including the case where the user is currently in the plan
     */
    public static boolean attendPlan(long userId, long planId) {
        Properties newAttend = new Properties();
        newAttend.setProperty(Attending.KEY_MEAL_PLAN_ID, Long.toString(planId));
        newAttend.setProperty(Attending.KEY_ATTENDEE_ID, Long.toString(userId));

        long insert = DBManager.insert(Attending.class, newAttend);
        AWS.getAWS(MainActivity.context, null).joinPlan(planId);

        return insert == 0;
    }

    /**
     * The user decides to leave a meal plan, remove the corresponding entry through database manager
     * @param userId the user's id
     * @param planId the plan's id
     * @return true if remove success
     *         false otherwise, including the case where the user is not in the plan originally
     */
    public static boolean leavePlan(long userId, long planId) {
        boolean success = DBManager.leavePlan(planId, userId);
        if (success)
            AWS.getAWS(MainActivity.context, null).leavePlan(planId);
        return success;
    }

    /**
     * Find a list of plans based on the query condition
     * @param hostname the host's name
     * @param hostGender the host gender
     * @param hostSchoolYear the host's school year
     * @param dateAndTime meal plan date and time
     * @param foodmood food mood
     * @return a list of plans which meet the query criteria
     *         an empty list if there is no matches
     *         null on error
     */
    public static List<MealPlan> query(String hostname, Character hostGender,
                                       Account.Year hostSchoolYear, Calendar dateAndTime, String foodmood) {

        List<MealPlan> res = new ArrayList<MealPlan>();

        Properties activeFilter = new Properties();

        if (hostname != null) {
            activeFilter.setProperty(MealPlan.KEY_HOSTNAME, hostname);
        }

        if (hostGender != null) {
            activeFilter.setProperty(Account.KEY_GENDER, Character.toString(hostGender));
        }

        if (hostSchoolYear != null) {
            activeFilter.setProperty(Account.KEY_YEAR, hostSchoolYear.getRepNumber() + "");
        }

        if (dateAndTime != null) {
            sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
            activeFilter.setProperty(MealPlan.KEY_TIME, sdf.format(dateAndTime.getTime()));
        }

        if (foodmood != null) {
            activeFilter.setProperty(MealPlan.KEY_FOODMOOD, foodmood);
        }

        List<Properties> planList = DBManager.query(MealPlan.class, activeFilter);

        if (planList == null) {
            return null;
        }

        for (Properties kvPair: planList) {
            res.add(extractMealPlan(kvPair));
        }
        return res;
    }

    /**
     * @return the MealPlan associated with the given plan id or null if it doesn't exist
     */
    public static MealPlan getPlan(long planId) {
        Properties filter = new Properties();
        filter.setProperty(MealPlan.KEY_ID, planId + "");

        List<Properties> list = DBManager.query(MealPlan.class, filter);
        if (list != null && list.size() == 1) {
            return extractMealPlan(list.get(0));
        }
        return null;
    }

    /**
     * Find a list of all plans
     * @return a list of all plans,
     *          an empty list if there is no available plans at the moment
     *          null on error
     */
    public static List<MealPlan> getAllPlans() {
        List<MealPlan> res = new ArrayList<MealPlan>();
        List<Properties> planList = DBManager.query(MealPlan.class, new Properties());
        if (planList == null) {
            return null;
        }

        for (Properties kvPairs: planList) {
            res.add(extractMealPlan(kvPairs));
        }
        return res;
    }

    /**
     * Gets a copy of the list of all the attendees' accounts
     * @param currentUserId the id of current user
     * @param planId the id of current meal plan
     * @param list an empty list to retrieve attendee accounts
     * @return 1 if the current user is in this plan
     *         0 if the current user is not in this plan
     *         -1 on error
     */
    public static int getAttendees(long currentUserId, long planId, List<Account> list) {
        int userIsInPlan = 0;
        Properties searchAttending = new Properties();
        searchAttending.setProperty(Attending.KEY_MEAL_PLAN_ID, Long.toString(planId));
        List<Properties> attendeeList = DBManager.query(Attending.class, searchAttending);
        if (attendeeList == null) {
            return -1;
        }
        Log.d("ATTENDEE LIST LIST LIST", attendeeList.toString());
        for (Properties attendee : attendeeList) {
            Long userId = Long.parseLong(attendee.getProperty(Account.KEY_ID));
            if (userId == currentUserId) {
                userIsInPlan = 1;
            }
            int yearInt = Integer.parseInt(attendee.getProperty(Account.KEY_YEAR));
            Account.Year year = Account.Year.toYear(yearInt);
            list.add(new Account(userId, attendee.getProperty(Account.KEY_EMAIL), attendee.getProperty(Account.KEY_NAME), year,
                     attendee.getProperty(Account.KEY_GENDER).charAt(0), attendee.getProperty(Account.KEY_MAJOR)));
        }
        return userIsInPlan;
    }

    /**
     * Private helper method does the conversion between Properties and MealPlan
     * @param kvPairs the Properties object used in DBManager
     * @return a MealPlan which has all the information stored in kvPairs
     */
    private static MealPlan extractMealPlan(Properties kvPairs) {
        String hostName = kvPairs.getProperty(MealPlan.KEY_HOSTNAME);
        long planId = Long.parseLong(kvPairs.getProperty(MealPlan.KEY_ID));
        long hostId = Long.parseLong(kvPairs.getProperty(MealPlan.KEY_HOST_ID));
        Calendar dateAndTime = gmtToLocal(kvPairs.getProperty(MealPlan.KEY_TIME));
        String foodMood = kvPairs.getProperty(MealPlan.KEY_FOODMOOD);
        String location = kvPairs.getProperty(MealPlan.KEY_LOCATION);
        String description = kvPairs.getProperty(MealPlan.KEY_DESCRIPTION);

        return new MealPlan(hostName, planId, hostId, dateAndTime, foodMood, location, description);
    }

    /**
     * Private helper function convert a date time string in GMT zone
     * into an calendar object with time in local time zone
     * @param date the String represents the time in GMT zone
     * @return a Calendar object indicates the converted time in local time zone
     */
    private static Calendar gmtToLocal(String date) {
        sdf.setTimeZone(TimeZone.getTimeZone("GMT"));
        Log.d(TAG, "MealPlan GMT: " + date);
        Calendar result = Calendar.getInstance();
        try {
            result.setTime(sdf.parse(date));
        } catch (ParseException e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
        }
        result.setTimeZone(TimeZone.getDefault());
        Log.d(TAG, "MealPlan Local: " + result.toString());
        return result;
    }

    /**
     * Retrieve a list of meal plans that this user is hosting
     * @param userId the user's id
     * @return a list of MealPlans that this user is hosting
     *         an empty list if the user doesn't host any meal plans
     *         null on error
     */
    public static List<MealPlan> getHostingList(long userId) {
        Properties whereKVs = new Properties();
        whereKVs.setProperty(MealPlan.KEY_HOST_ID, Long.toString(userId));
        List<Properties> planList = DBManager.query(MealPlan.class, whereKVs);
        if (planList == null) {
            return null;
        }

        List<MealPlan> res = new ArrayList<MealPlan>();
        for (Properties kvPairs: planList) {
            res.add(extractMealPlan(kvPairs));
        }
        return res;
    }

    /**
     * Retrieve a list of meal plans that this user is attending (not hosting)
     * based on the passed in userId
     * @param userId the user's id
     * @return a list of MealPlans that this user is attending
     *         an empty list if the user doesn't attend any plans
     *         null on error
     */
    public static List<MealPlan> getAttendingList(long userId) {
        Properties whereKVs = new Properties();
        whereKVs.setProperty(Attending.KEY_ATTENDEE_ID, Long.toString(userId));
        List<Properties> planProperties = DBManager.query(Attending.class, whereKVs);
        if (planProperties == null) {
            return null;
        }

        List<MealPlan> res = new ArrayList<MealPlan>();
        for (Properties kvPairs: planProperties) {
            res.add(extractMealPlan(kvPairs));
        }
        return res;
    }
}
