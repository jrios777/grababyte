package edu.uw.cs.meetagenius;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * <b>DBManager</b> represents the abstraction through which all backend communication will be done.
 * This class is non-instantiable and should be used as a utility class.
 *
 * @author Josue Rios
 */
public final class DBManager {
    // static constants for query
    private static final String stemUrl = "http://ec2-52-11-250-252.us-west-2.compute.amazonaws.com/android_connect";
    private static final String url_query_plans = stemUrl + "/query_plans.php";
    private static final String url_query_attending = stemUrl + "/query_attending.php";
    private static final String url_query_account = stemUrl + "/query_account.php";
    private static final String url_insert_plan = stemUrl + "/create_plan.php";
    private static final String url_insert_attending = stemUrl + "/insert_attending.php";
    private static final String url_remove_account = stemUrl + "/remove_account.php";
    private static final String url_remove_attending = stemUrl + "/remove_attending.php";
    private static final String url_update_account = stemUrl + "/update_account.php";
    private static final String TAG = DBManager.class.getSimpleName();  // for debugging

    // Doesn't make sense to have more than one DBManager
    private DBManager() { throw new AssertionError(""); }

    /**
     * Inserts the given values into the given table in the database.
     * @param table the table to insert into.
     * @param properties the attribute-value properties to insert into the table
     * @return the id of the insertion, 0 if added to a relationship, -1 on error
     */
    public static long insert(Class<? extends Table> table, Properties properties) {
        try {
            String baseUrl;
            if (table.getName().equals(MealPlan.class.getName()))
                baseUrl = url_insert_plan;
            else if (table.getName().equals(Attending.class.getName()))
                baseUrl = url_insert_attending;
            else {
                Log.d(TAG, "Given class: " + table.getName() + "not " + MealPlan.class.getName() +
                        " nor " + Attending.class.getName());
                return -1;
            }

            // Connect and parse the response
            HttpURLConnection connection = connect(baseUrl, "POST", properties);
            JsonReader reader = parseResponse(connection);
            if (reader == null)
                return -1;
            else {
                String message = reader.nextName();
                String messageValue = reader.nextString();
                Log.d(TAG, message + ":" + messageValue);
                reader.nextName();
                return Long.parseLong(reader.nextString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return -1;
        }
    }

    /**
     * Removes this person from the database and leaves them from any plans they host or attend.
     * The next time they try to log in, their account information is reset.
     * @param userId the id of the user to remove.
     * @return true if the removal succeeded, false on failure.
     */
    public static boolean deleteAccount(long userId) {
        try {
            Properties properties = new Properties();
            properties.setProperty(Account.KEY_ID, Long.toString(userId));

            // Connect and parse the response
            HttpURLConnection connection = connect(url_remove_account, "POST", properties);
            return parseResponse(connection) != null;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return false;
        }
    }

    /**
     * Removes the person with the given personId from the given plan, regardless of whether they
     * are the host or not. If a plan has other attendees and the host is removed, the hostid will be
     * assigned to another attendee. Otherwise if the host is removed and there were no attendees
     * the plan will also be removed.
     * @param planId the id of the plan.
     * @param personId the id of the attendee/host to remove.
     * @return true if the removal succeeded, false on failure.
     */
    public static boolean leavePlan(long planId, long personId) {
        try {
            Properties properties = new Properties();
            properties.setProperty(Attending.KEY_MEAL_PLAN_ID, Long.toString(planId));
            properties.setProperty(Attending.KEY_ATTENDEE_ID, Long.toString(personId));

            // Connect and parse the response
            HttpURLConnection connection = connect(url_remove_attending, "POST", properties);
            return parseResponse(connection) != null;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return false;
        }
    }

    /**
     * Updates the given table to reflect changes of the passed in values where the id
     * matches the given id. (Meant for MealPlan or Account)
     * @param id the primary key id of the table to update
     * @param properties the key-value properties to update (DO NOT INCLUDE id HERE)
     * @pre table should be Account.
     * @return true if update was successful, false otherwise.
     */
    public static boolean update(Class<? extends Table> table, long id, Properties properties) {
        try {
            String baseUrl;
            if (table.getName().equals(Account.class.getName()))
                baseUrl = url_update_account;
            else {
                Log.d(TAG, "Given class: " + table.getName() + "not " + Account.class.getName());
                return false;
            }
            // Add the id property, connect and parse the response
            properties.setProperty(Account.KEY_ID, Long.toString(id));
            HttpURLConnection connection = connect(baseUrl, "POST", properties);
            return parseResponse(connection) != null;
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage());
            return false;
        }
    }

    /**
     * Queries the given table to for all values where the given keys match the given values. If the
     * given table is MealPlan, plans will be automatically sorted by date. If the given table is
     * Account, then if the email doesn't exist it is automatically created.
     * @param table the table to get results from
     * @param filterProperties the key-value properties of the results you're looking for. In general,
     *                        keys should be the KEY constants in the given table's class. For MealPlan
     *                        however, it is permissible to additionally filter by host gender, year,
     *                        and major.
     * @return a list of object properties client can use to grab information, or null on error. <br>
     *     Specifically, KEY constants,<br>
     *         <b>MealPlan</b>: id, hostid, hostname, time, foodmood, location, description <br>
     *         <b>Account</b>: id, email, name, year, gender, major, activated, validationCode <br>
     *         <b>Attending</b>: Depends on whether you're querying by planid or by attendeeid: <br>
     *             By attendeeid: id, hostid, hostname, time, foodmood, location, description <br>
     *             By planid: attendeeid, attendeename, email, gender, major, year
     */
    public static List<Properties> query(Class<? extends Table> table, Properties filterProperties) {
        try {
            System.err.println("in DBManager");
            String baseUrl;
            if (table.getName().equals(MealPlan.class.getName()))
                baseUrl = url_query_plans;
            else if (table.getName().equals(Account.class.getName()))
                baseUrl = url_query_account;
            else if (table.getName().equals(Attending.class.getName()))
                baseUrl = url_query_attending;
            else {
                Log.d(TAG, "Given class: " + table.getName() + "not " + MealPlan.class.getName() + " or " + Account.class.getName());
                System.err.println(TAG + "Given class: " + table.getName() + "not " + MealPlan.class.getName() + " or " + Account.class.getName());
                return null;
            }

            // Connect and parse the response
            HttpURLConnection connection = connect(baseUrl, "GET", filterProperties);
            JsonReader reader = parseResponse(connection);
            if (reader == null) return null;    // Some bad response
            return parseJsonQueryResults(reader);
        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, e.getMessage() + "");
            return null;
        }
    }

    /**
     * Parses the given properties into the params string
     * @param properties the key-value pairs of the WHERE clause
     * @return string representing the given properties
     * @throws UnsupportedEncodingException if certain characters can't be converted
     */
    private static String getParamsString(Properties properties) throws UnsupportedEncodingException {
        StringBuilder params = new StringBuilder();
        for (Object k: properties.keySet()) {
            String key = (String) k;
            params.append(key);
            params.append('=');
            params.append(URLEncoder.encode(properties.getProperty(key), "UTF-8"));
            params.append('&');
        }
        Log.d(TAG, "params: " + params);
        return params.toString();
    }

    /**
     * Creates a connection to the url using the given method and parameters.
     * @param baseUrl the base url to connect to
     * @param method the method of request
     * @return a connection to the given url with default values
     * @throws IOException on I/O failure
     */
    private static HttpURLConnection connect(String baseUrl, String method, Properties params)
            throws IOException {
        String filter = getParamsString(params);
        if (method.equals("GET"))
            baseUrl += "?" + filter;
        Log.d(TAG, "\nURL: " + baseUrl + "\nMethod: " + method + "\nParams: " + params);

        URL url = new URL(baseUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();
        connection.setReadTimeout(10000 /* milliseconds */);
        connection.setConnectTimeout(15000 /* milliseconds */);
        connection.setRequestMethod(method);
        if (method.equals("GET")) {
            connection.setDoInput(true);
            connection.connect();
        } else {    // POST
            connection.setDoOutput(true);
            connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded");
            connection.setRequestProperty("charset", "UTF-8");
            OutputStream output = connection.getOutputStream();
            output.write(filter.getBytes("UTF-8"));
            Log.d(TAG, "Wrote out: " + filter);
        }
        return connection;
    }

    /**
     * Returns the JsonReader on successful HTTP and JSON response.
     * @param connection the connection to read the responses from
     * @throws IOException when something goes wrong with I/O
     * @return a new JsonReader on success or null if any response indicates error
     */
    private static JsonReader parseResponse(HttpURLConnection connection) throws IOException {
        // Check the response tag
        int responseCode = connection.getResponseCode();
        Log.v(TAG, "HTTP response: " + responseCode);

        // Check the JSON response
        JsonReader reader = new JsonReader(new InputStreamReader(connection.getInputStream(), "UTF-8"));
        reader.beginObject();
        String successKey = reader.nextName();
        int successValue = reader.nextInt();
        Log.v(TAG, successKey + ": " + successValue);
        if (successValue == 0) {
            // Some sort of error
            reader.nextName();
            String message = reader.nextString();
            Log.v(TAG, "message: " + message);
//            reader.ne
//            int errno = Integer.parseInt(reader.nextString());
//            Log.v(TAG, "errno: " + errno);
            return null;
        }

        return reader;  // sucess, return the reader
    }

    /**
     * Private method that returns a list of object properties client can use to grab information
     * from the JSON query results
     * @param reader the reader to parse the JSON from
     * @return a list of object properties client can use to grab information
     * @throws IOException on some I/O failure.
     */
    private static List<Properties> parseJsonQueryResults(JsonReader reader) throws IOException {
        List<Properties> results = new ArrayList<>();
        Log.d(TAG, "Parsing Array");
        reader.nextName();  // name consist of "results"
        reader.beginArray();    // bring "results" array
        while (reader.hasNext()) {
            Properties nextObject = parseJsonObject(reader);
            results.add(nextObject);
        }
        reader.endArray();

        return results;
    }

    /**
     * Private method that returns the properties of a single object read from the reader
     * @param reader the reader to parse the JSON from
     * @return the properties of a single object read from the reader
     * @throws IOException on some I/O failure.
     */
    private static Properties parseJsonObject(JsonReader reader) throws IOException {
        Properties result = new Properties();
        Log.d(TAG, "Parsing Object");
        // read in the properties of this object
        reader.beginObject();
        while (reader.hasNext()) {
            String key = reader.nextName();
            if (!reader.peek().equals(JsonToken.NULL)) {
                String value = reader.nextString();
                result.setProperty(key, value);
            } else {
                reader.skipValue();
                result.setProperty(key, "");
            }
        }
        reader.endObject();

        // After reading in an object, return this
        return result;
    }
}