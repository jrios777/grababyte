package edu.uw.cs.meetagenius;

import android.content.Context;
import android.content.SharedPreferences;

import com.amazonaws.auth.CognitoCachingCredentialsProvider;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.CreatePlatformEndpointRequest;
import com.amazonaws.services.sns.model.GetEndpointAttributesRequest;
import com.amazonaws.services.sns.model.GetEndpointAttributesResult;
import com.amazonaws.services.sns.model.NotFoundException;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.SetEndpointAttributesRequest;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.val;


/**
 * This class handles dealing with AWS
 *
 * Created by Josue Rios on 8/20/15.
 */
public class AWS {
    private static AWS aws;
    private static final String TAG = "AWS";

    /** do not run on ui thread*/
    public static AWS getAWS(Context context, String token) {
        if (aws == null)
            aws = new AWS(context, token);
        return aws;
    }

    private static <T> T coalesce(T a, T b) { return (a == null) ? b : a; }

    private static void start(Runnable runnable) {
        Thread thread = new Thread(runnable);
        // We want to log exceptions when they happen
        thread.setUncaughtExceptionHandler((t, e) -> Log.e(TAG, "Exception in Thread", e));
        thread.start();
    }

    // Cognito
    private static final String COGNITO_POOL_ID = "us-east-1:30ad037c-e416-4a3c-a38c-c61304400fbe";
    private static final Regions COGNITO_REGION = Regions.US_EAST_1;

    // SNS
    private static final String APP_ARN = "arn:aws:sns:us-east-1:542355837303:app/GCM/MeetAGenius";

    // Preference constants
    private static final String PREFS_NAME = "aws",
                                KEY_TOKEN = "deviceToken",
                                KEY_ENDPOINT = "endpointArn",
                                KEY_PLANS = "plans";

    // Notification keys
    public static final String  KEY_PLAN_ID = "planID",
                                KEY_TITLE = "title",
                                KEY_MESSAGE = "alert";

    private Context context;
    private SharedPreferences prefs;
    private AmazonSNS sns;
    private CognitoCachingCredentialsProvider credsProvider;
    private String endpointArn;

    private AWS(Context context, String token) {
        this.context = context;
        prefs = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
        setupCognito();
        endpointArn = prefs.getString(KEY_ENDPOINT, null);
        if (token != null) {
            registerWithSNS(token);
        }
    }

    private void setupCognito() {
        credsProvider = new CognitoCachingCredentialsProvider(context, COGNITO_POOL_ID, COGNITO_REGION);
        String cognitoId = coalesce(credsProvider.getCachedIdentityId(), credsProvider.getIdentityId());
        Log.d(TAG, "Cognito ID:" + cognitoId);
    }

    private void registerWithSNS(String token) {
        prefs.edit().putString(KEY_TOKEN, token).apply();
        sns = new AmazonSNSClient(credsProvider);
        if (endpointArn == null) {
            // first time creating the application endpoint. Create and subscribe it?
            createEndpoint(token);
        }
        Log.d(TAG, "Endpoint Arn: " + endpointArn);

        // Create the Get Endpoint Attributes request
        val request = new GetEndpointAttributesRequest().withEndpointArn(endpointArn);

        // Get the attributes
        try {
            GetEndpointAttributesResult result = sns.getEndpointAttributes(request);

            Map<String,String> attributes = result.getAttributes();
            if (!attributes.get("Token").equals(token) || !attributes.get("Enabled").equals("true")) {
                // Something's out of sync
                attributes.put("Token", token);
                attributes.put("Enabled", "true");
                sns.setEndpointAttributes(new SetEndpointAttributesRequest()
                        .withEndpointArn(endpointArn)
                        .withAttributes(attributes));
            }
        } catch (NotFoundException e) {
            // We need to recreate the endpointArn
            createEndpoint(token);
        }
    }

    /** Creates a platform application endpoint for this device*/
    private void createEndpoint(String token) {
        val request = new CreatePlatformEndpointRequest()
                .withPlatformApplicationArn(APP_ARN)
                .withToken(token);
        endpointArn = sns.createPlatformEndpoint(request).getEndpointArn();
        prefs.edit().putString(KEY_ENDPOINT, endpointArn).apply();
    }

    public void createPlan(long planID) {
        String topicName = "P" + planID;
        String topicArn = sns.createTopic(topicName).getTopicArn();
        String subscriptionArn = sns.subscribe(topicArn, "application", endpointArn).getSubscriptionArn();
        PlanSubscription p = new PlanSubscription(topicName, subscriptionArn);
        if (!topicArn.equals(p.getTopicArn()))
            throw new RuntimeException("CHECK YOUR IMPLEMENTATION, you thought " + topicArn + " was " + p.getTopicArn());
        Log.d(TAG, "Created Plan topic with arn:" + topicArn);
    }

    public void joinPlan(long planID) {
        // Same behavior. Works since createTopic will return existing topic rather than wipe the old one
        String topicName = "P" + planID;
        String topicArn = sns.createTopic(topicName).getTopicArn();
        String subscriptionArn = sns.subscribe(topicArn, "application", endpointArn).getSubscriptionArn();
        PlanSubscription p = new PlanSubscription(topicName, subscriptionArn);
        String msg = AccountManager.getUserName() + " joined your meal plan!";
        publish(p.getTopicArn(), msg, planID);
        Log.d(TAG, "Subscribed to: " + topicArn);
    }

    public void leavePlan(long planID) {
        PlanSubscription p = new PlanSubscription("P" + planID);
        sns.unsubscribe(p.subscriptionArn);
        String msg = AccountManager.getUserName() + " left your meal plan";
        publish(p.getTopicArn(), msg, planID);
        Log.d(TAG, "Unsubscribed from:" + p.getTopicArn());
    }

    private void publish(String topicArn, String message, long planID) {
        try {
            // Principal
            JSONObject data = new JSONObject()
                    .put("alert", message)
                    .put("pid", planID);

            // Platform dictionaries
            JSONObject gcm = new JSONObject().put("data", data);
            JSONObject apns = new JSONObject().put("aps", data);

            // Overall
            JSONObject json = new JSONObject()
                    .put("default", message)
                    .put("GCM", gcm.toString())
                    .put("APNS", apns.toString())
                    .put("APNS_SANDBOX", apns.toString());

            Log.d(TAG, "Generated JSON:\n" + json.toString(2));
            message =  json.toString();
        } catch (JSONException e) {
            Log.e(TAG, e.getMessage(), e);
            message =  "{ \\\"default\\\":\\\""+message+"\\\"}";
        }
        sns.publish(new PublishRequest(topicArn, message).withMessageStructure("json"));
    }

    @AllArgsConstructor
    private class PlanSubscription {
        private final String topicName, subscriptionArn;

        public PlanSubscription(String topicName) {
            if (!prefs.contains(topicName))
                throw new IllegalArgumentException("Topic " + topicName + " was never stored");
            subscriptionArn = prefs.getString(topicName, "");
            this.topicName = topicName;
        }

        private String getTopicArn() {
            return "arn:aws:sns:us-east-1:542355837303:" + topicName;
        }

        public void save() {
            Set<String> plans = prefs.getStringSet(KEY_PLANS, new HashSet<>());
            plans.add(topicName);
            prefs.edit().putString(topicName, subscriptionArn).apply();
            prefs.edit().putStringSet(KEY_PLANS, plans).apply();
        }
    }
}
