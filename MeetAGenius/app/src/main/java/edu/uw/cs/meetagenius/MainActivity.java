package edu.uw.cs.meetagenius;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.ViewGroup;
import android.widget.Toast;

import java.lang.CharSequence;
import java.lang.Override;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * Represents the main activity of the app. Contains the three tabs.
 *
 * Created by jessicali on 4/23/15.
 */

public class MainActivity extends FragmentActivity implements ActionBar.TabListener {

    public static Context context;

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide fragments for each of the
     * three primary sections of the app. We use a {@link android.support.v4.app.FragmentPagerAdapter}
     * derivative, which will keep every loaded fragment in memory. If this becomes too memory
     * intensive, it may be best to switch to a {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    AppSectionsPagerAdapter mAppSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will display the three primary sections of the app, one at a
     * time.
     */
    ViewPager mViewPager;

    /*
     * Called when the activity is starting.
     *
     * @param savedInstanceState If the activity is being re-initialized after previously being shut
     *                           down then this Bundle contains the data it most recently supplied in
     *                           onSaveInstanceState(Bundle). Otherwise it is null.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = this;
        setContentView(R.layout.activity_temp_main);

        // Create the adapter that will return a fragment for each of the three primary sections
        // of the app.
        mAppSectionsPagerAdapter = new AppSectionsPagerAdapter(getSupportFragmentManager());

        // Set up the action bar.
        final ActionBar actionBar = getActionBar();

        // Specify that the Home/Up button should not be enabled, since there is no hierarchical
        // parent.
        actionBar.setHomeButtonEnabled(false);

        // Specify that we will be displaying tabs in the action bar.
        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        // Set up the ViewPager, attaching the adapter and setting up a listener for when the
        // user swipes between sections.
        mViewPager = (ViewPager) findViewById(R.id.pager);
        mViewPager.setOffscreenPageLimit(2);    // This will make sure the tabs are always loaded
        mViewPager.setAdapter(mAppSectionsPagerAdapter);
        mViewPager.setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
            @Override
            public void onPageSelected(int position) {
                // When swiping between different app sections, select the corresponding tab.
                // We can also use ActionBar.Tab#select() to do this if we have a reference to the
                // Tab.
                actionBar.setSelectedNavigationItem(position);
            }
        });

        // For each of the sections in the app, add a tab to the action bar.
        for (int i = 0; i < mAppSectionsPagerAdapter.getCount(); i++) {
            // Create a tab with text corresponding to the page title defined by the adapter.
            // Also specify this Activity object, which implements the TabListener interface, as the
            // listener for when this tab is selected.
            actionBar.addTab(
                    actionBar.newTab()
                            .setText(mAppSectionsPagerAdapter.getPageTitle(i))
                            .setTabListener(this));
        }

        int position = getIntent().getIntExtra("tab", 0);
        mViewPager.setCurrentItem(position);
    }

    /* This method is called to create the options menu. It initializes
     * the contents within the menu.
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.items, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /*
    This method is called when the back button is pressed. It exits the app and goes to the
    home window.
     */
    @Override
    public void onBackPressed() {
        Intent a = new Intent(Intent.ACTION_MAIN);
        a.addCategory(Intent.CATEGORY_HOME);
        a.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(a);
    }

    /* This method is called when an item in the options menu is selected. The
         * menu contains "Refresh", "Settings", "Help", "Report a Bug" and "Logout".
         */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        super.onOptionsItemSelected(item);
        switch(item.getItemId()) {
            case R.id.refresh:
                // refresh the page to get new information from the database
                Intent intent = new Intent(this, MainActivity.class);
                intent.putExtra(Account.KEY_GENDER, (Character) null);
                intent.putExtra(MealPlan.KEY_HOSTNAME, (String) null);
                intent.putExtra(MealPlan.KEY_TIME, (Calendar) null);
                intent.putExtra(MealPlan.KEY_FOODMOOD, (String) null);
                intent.putExtra(Account.KEY_YEAR, (Account.Year) null);
                intent.putExtra("tab", mViewPager.getCurrentItem());
                startActivity(intent);
                break;
            case R.id.help:
                // go to Help page if there is internet connection
                if (NetworkConnection.checkConnection(this)) {
                    String url = "http://homes.cs.washington.edu/~jrios777/cse403/web/product/help.html";
                    Intent launchBrowser = new Intent(Intent.ACTION_VIEW);
                    launchBrowser.setData(Uri.parse(url));
                    startActivity(launchBrowser);
                } else {
                    Toast.makeText(this, "Check internet connection.", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.report_a_bug:
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                sendIntent.setType("plain/text");
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "Report a problem");
                // sends email to all developers of this app
                sendIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"meetageniusdevelopers@gmail.com"});
                startActivity(sendIntent);
                break;
            case R.id.logout:
                // log the user out and return to Log in/Sign up page
                AccountManager.logout();
                // Set logout value in shared preferences to true
                SharedPreferences sharedPref = getApplicationContext().
                        getSharedPreferences(getString(R.string.has_logged_out), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.has_logged_out), 1);
                editor.commit();

                int logout = sharedPref.getInt(getString(R.string.has_logged_out), 0);

                intent = new Intent(this, LogInActivity.class);
                // Closing all the Activities
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                // Add new Flag to start new Activity
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                this.startActivity(intent);
                finish();
                break;
        }

        return true;
    }

    /*
     * Called when a tab exits the selected state. Nothing happens when this occurs.
     * @param tab The tab that was unselected
     * @param fragmentTransaction A FragmentTransaction for queuing fragment operations to execute
     *                            during a tab switch.
     */
    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /*
     * Called when a tab enters the selected state. This causes the view to switch to the
     * corresponding page in the ViewPager
     *
     * @param tab The tab that was selected
     * @param fragmentTransaction A FragmentTransaction for queuing fragment operations to execute
     *                            during a tab switch.
     */
    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
        // When the given tab is selected, switch to the corresponding page in the ViewPager.
        mViewPager.setCurrentItem(tab.getPosition());
    }

    /*
     * Called when a tab that is already selected is chosen again by the user. This does nothing.
     *
     * @param tab The tab that was reselected.
     * @param fragmentTransaction A FragmentTransaction for queuing fragment operations to execute
     *                            once this method returns.
     */
    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction fragmentTransaction) {
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to one of the primary
     * sections of the app.
     */
    public static class AppSectionsPagerAdapter extends FragmentPagerAdapter {

        /**
         * Creates a new AppsSectionPagerAdapter
         *
         * @param fm  The FragmentManager that keeps track of each page
         */
        public AppSectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        /*
         * Return the Fragment associated with a specified position.
         *
         * @param i The index of the fragment to return
         * @return Fragment that is at the index i
         */
        @Override
        public Fragment getItem(int i) {
            switch (i) {
                case 0:
                    // The 1st section of the app is Home (feed of meal plans)
                    return new HomeFragment();
                case 1:
                    // The 2nd section of the app is My Meal Plans
                    // (list of meal plans that user is attending/hosting)
                    Fragment fragment1 = new MyMealPlanFragment();
                    Bundle args1 = new Bundle();
                    args1.putInt(MyMealPlanFragment.ARG_SECTION_NUMBER, i + 1);
                    fragment1.setArguments(args1);
                    return fragment1;
                default:
                    // The 3rd section of the app is My Account (manage account)
                    Fragment fragment2 = new ManageAccountFragment();
                    Bundle args2 = new Bundle();
                    args2.putInt(edu.uw.cs.meetagenius.ManageAccountFragment.ARG_SECTION_NUMBER, i + 1);
                    fragment2.setArguments(args2);
                    return fragment2;
            }
        }

        /*
         * Given a object, will return POSITION_NONE (which means it is no longer
         * in the adapter.
         */
        @Override
        public int getItemPosition(Object object) {
            return POSITION_NONE;
        }

        /*
         * Returns the count of fragments
         *
         * @return count of fragments
         */
        @Override
        public int getCount() {
            return 3;
        }

        /*
         *  Returns the name of the tab at the given position
         */
        @Override
        public CharSequence getPageTitle(int position) {
            if (position == 0) {
                return "Home";
            } else if (position == 1) {
                return "My Meal Plans";
            } else {
                return "My Account";
            }
        }

    }

}
