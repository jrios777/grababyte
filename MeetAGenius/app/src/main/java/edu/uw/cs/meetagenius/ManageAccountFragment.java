package edu.uw.cs.meetagenius;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

/**
 * Fragment for Managing User Account
 *
 * Created by jessicali on 4/23/15.
 */
public class ManageAccountFragment extends Fragment {

    /** The user's name */
    private EditText name;
    /** The user's major */
    private EditText major;
    /** The user's email */
    private EditText email;
    /** The user's year */
    private Spinner year;
    /** The user's gender */
    private Spinner gender;

    /** A key used for tab navigation when handling intents */
    public static final String ARG_SECTION_NUMBER = "section_number";

    /*
     * Called to have the fragment instantiate its user interface view
     *
     * @param inflater The LayoutInflater object that can be used to inflate any views in the fragment
     * @param container If non-null, this is the parent view that the fragment's UI should be attached to
     * @param savedInstanceState If non-null, this fragment is being re-constructed from a previous
     *                           saved state as given here.
     * @return Return the View for the fragment's UI, or null.
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            final Bundle savedInstanceState) {
        final View rootView = inflater.inflate(R.layout.fragment_manage_account, container, false);
        return rootView;

    }

    /*
     * Called when the fragment is visible to the user and actively running.
     * Sets up the initial display of the account information.
     */
    @Override
    public void onResume() {
        super.onResume();
        final View rootView = getView();
        name = (EditText) rootView.findViewById(R.id.account_name);
        major = (EditText) rootView.findViewById(R.id.account_major);
        email = (EditText) rootView.findViewById(R.id.account_email);
        year = (Spinner) rootView.findViewById(R.id.account_year);
        gender = (Spinner) rootView.findViewById(R.id.account_gender);

        // get the name, email, major, school year, and gender of the account from AccountManager
        // and display the initial information if not null

        if (AccountManager.getUserName() != null) {
            name.setText(AccountManager.getUserName());
        }
        // email should never be null
        if (AccountManager.getUserEmail() != null) {
            email.setText(AccountManager.getUserEmail());
        }
        if (AccountManager.getUserMajor() != null) {
            major.setText(AccountManager.getUserMajor());
        }
        Account.Year schoolYear = AccountManager.getUserSchoolYear();
        if (schoolYear != null) {
            year.setSelection(schoolYear.getRepNumber());
        }
        year.setEnabled(false);

        Character genderChar = AccountManager.getUserGender();
        gender.setSelection(2); // set the selection for gender to be Prefer not to disclose
        if (genderChar =='M') {
            gender.setSelection(0);
        } else if (genderChar == 'F') {
            gender.setSelection(1);
        }
        gender.setEnabled(false);

        Button deleteButton = (Button) rootView.findViewById(R.id.delete_account_button);
        Button editButton = (Button) rootView.findViewById(R.id.edit_account_button);
        Button saveButton = (Button) rootView.findViewById(R.id.save_account_changes_button);
        Button cancelButton = (Button) rootView.findViewById(R.id.cancel_account_changes_button);

        deleteButton.setOnClickListener(v -> {
            // dialog that alerts user that they are deleting their account
            new AlertDialog.Builder(getActivity())
                    .setTitle("Warning")
                    .setMessage(R.string.delete_confirmation_message)
                    .setPositiveButton("Confirm", (d, which) -> {
                        // if user clicks Confirm, delete account and start Login Activity
                        new DeleteAccountTask().execute();
                        d.dismiss();
                    })
                    .setNegativeButton(android.R.string.cancel, (d, which) -> d.dismiss())
                    .create()
                    .show();
        });

        editButton.setOnClickListener(v -> {
            // if user clicks Edit Button, change widgets to be editable and display
            // Save and Cancel buttons
            setEditable(name);
            setEditable(major);
            year.setEnabled(true);
            gender.setEnabled(true);

            saveButton.setVisibility(View.VISIBLE);
            cancelButton.setVisibility(View.VISIBLE);
        });

        saveButton.setOnClickListener(v -> {
            // if user clicks Save Button, update user account in database
            if (NetworkConnection.checkConnection(getActivity()))
                new UpdateAccountTask().execute();
            else
                Toast.makeText(getActivity(), "Check internet connection.", Toast.LENGTH_SHORT).show();
        });

        cancelButton.setOnClickListener(v -> {
            // if user clicks Cancel Button, don't save changes and refresh Fragment
            Toast.makeText(getActivity(), "Your changes have been cancelled", Toast.LENGTH_SHORT).show();

            getActivity().recreate();
            getActivity().getIntent().putExtra("tab", 2);
        });

    }

    /**
     * Sets the given EditText to be editable
     *
     * @param et The EditText to set editable
     */
    private void setEditable(EditText et) {
        et.setEnabled(true);
        et.setFocusable(true);
        et.setClickable(true);
        et.setFocusableInTouchMode(true);
        et.setCursorVisible(true);
    }

    /**
     * Updates the account information in the database
     */
    public class UpdateAccountTask extends AsyncTask<Void, Void, Boolean> {

        /* Updates the account information in the background. Returns whether
         * the account update was successful or not */
        @Override
        protected Boolean doInBackground(Void... params) {
            // get the information from the widgets and save them in the database
            String nameValue = name.getText().toString();
            String majorValue = major.getText().toString();
            Character genderValue = gender.getSelectedItem().toString().charAt(0);

            int yearPosition = year.getSelectedItemPosition();
            Account.Year yearValue = Account.Year.toYear(yearPosition);

            return AccountManager.updateAccount(nameValue, yearValue, genderValue, majorValue);
        }

        /* This method is called after attempting to update the account information in the
         * database. If successful, will display a success message and ref. If fail, Will tell the
         * user to try again and check the internet connection. */
        @Override
        protected void onPostExecute(Boolean success) {
            super.onPostExecute(success);
            if (success) {
                // account information updated successfully
                Toast.makeText(getActivity(), "Your information has been saved", Toast.LENGTH_SHORT).show();
                getActivity().recreate();
                getActivity().getIntent().putExtra("tab", 2);
            } else {
                // account information update failed
                Toast.makeText(getActivity(), "Your information couldn't be saved. Please check connection.", Toast.LENGTH_SHORT).show();
            }
        }
    }

    /**
     * Deletes the user account in an asynchronous task
     */
    public class DeleteAccountTask extends AsyncTask<Void, Void, Boolean> {

        /*
         * Calls the AccountManager to delete the user account
         */
        @Override
        protected Boolean doInBackground(Void...params) {
            return AccountManager.deleteAccount();
        }

        /*
         * If result is true, logs the user out. Otherwise displays a Toast with a friendly error message.
         */
        @Override
        protected void onPostExecute(Boolean result) {
            if (result) {
                // set share pref logout value to true
                SharedPreferences sharedPref = getActivity().getApplicationContext().
                        getSharedPreferences(getString(R.string.has_logged_out), Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putInt(getString(R.string.has_logged_out), 1);
                editor.commit();
                Toast.makeText(getActivity(), "Your account has been deleted", Toast.LENGTH_LONG).show();
                Intent intent = new Intent(getActivity(), LogInActivity.class);
                startActivity(intent);
            } else {
                Toast.makeText(getActivity(), "Account not deleted. Please try again.", Toast.LENGTH_LONG).show();
            }
        }
    }
}

