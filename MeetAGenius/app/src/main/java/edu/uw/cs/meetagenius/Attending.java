package edu.uw.cs.meetagenius;

/**
 * This class contains the table name and column names for the
 * Attending table in the database.
 *
 * Created by jrios777 on 5/7/15.
 */
public class Attending implements Table {
    public static final String KEY_MEAL_PLAN_ID = "planid";
    public static final String KEY_ATTENDEE_ID = "attendeeid";
}
