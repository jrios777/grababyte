package edu.uw.cs.meetagenius;

import java.util.List;
import java.util.Properties;

/**
 * AccountManager deals with the interaction between Accounts and the Database.
 * Each AccountManager keeps track of one local Account.
 * This class is non-instantiable and should be used as a utility class.
 *
 * @author Junhao Zhu
 */
public class AccountManager {

    /** Each account manager keeps track of the current user. Namely, the local user from this phone. */
    private static Account currentUser;

    /**
     * Login attempt, accepts user's email, validate with the database, return the status
     * @param email user's email
     * @return true on login success, false otherwise
     */
    public static boolean login(String email) {
        Properties accountProperties = new Properties();
        accountProperties.setProperty(Account.KEY_EMAIL, email);
        Log.d("AccountManager", email);
        List<Properties> res = DBManager.query(Account.class, accountProperties);

        if (res != null && res.size() == 1) {
            Log.d("AccountManager", "Found the user in the DB");
            accountProperties = res.get(0);
            currentUser = new Account(Long.parseLong(accountProperties.getProperty(Account.KEY_ID)),
                    accountProperties.getProperty(Account.KEY_EMAIL),
                    accountProperties.getProperty(Account.KEY_NAME),
                    Account.Year.toYear(Integer.parseInt(accountProperties.getProperty(Account.KEY_YEAR))),
                    accountProperties.getProperty(Account.KEY_GENDER).charAt(0),
                    accountProperties.getProperty(Account.KEY_MAJOR));
            return true;
        } else {
            // Non existent or duplicate accounts
            Log.d("AccountManager", "Had some issues");
            return false;
        }
    }

    /**
     * logs the user out of Application and returns whether they were successfully logged out
     */
    public static void logout() {
        currentUser = null;
    }

    /**
     * Get the id of current user
     * @return id of current user
     */
    public static long getUserId() {
        return currentUser.getId();
    }

    /**
     * Get the name of current user
     * @return name of current user
     */
    public static String getUserName() {
        return currentUser.getName();
    }

    /**
     * Get the email address of current user
     * @return email address of current user
     */
    public static String getUserEmail() {
        return currentUser.getEmail();
    }

    /**
     * Get the school year of current user
     * @return school year of current user
     */
    public static Account.Year getUserSchoolYear() {
        return currentUser.getSchoolYear();
    }

    /**
     * Get the gender of current user
     * @return gender of current user
     */
    public static char getUserGender() {
        return currentUser.getGender();
    }

    /**
     * Get the major of current user
     * @return major of current user
     */
    public static String getUserMajor() {
        return currentUser.getMajor();
    }

    /**
     * Special utility function for retrieving host's info in a meal plan
     * @param userId the host's id
     * @return an Account object consisting of all info of the host
     *         null if the id does not exist, or on error
     *         in this case if the number of query results is greater than 1
     *         then it should be an error since user ids are unique
     */
    public static Account getUserById(long userId) {
        Properties accountSearch = new Properties();
        accountSearch.setProperty(Account.KEY_ID, Long.toString(userId));
        List<Properties> accountList = DBManager.query(Account.class, accountSearch);

        if (accountList == null || accountList.size() != 1) {
            return null;
        } else {
            Properties resAccount = accountList.get(0);

            int yearInt = Integer.parseInt(resAccount.getProperty(Account.KEY_YEAR));
            Account.Year year = Account.Year.toYear(yearInt);
            return new Account(userId,
                    resAccount.getProperty(Account.KEY_EMAIL),
                    resAccount.getProperty(Account.KEY_NAME),
                    year,
                    resAccount.getProperty(Account.KEY_GENDER).charAt(0),
                    resAccount.getProperty(Account.KEY_MAJOR));
        }
    }

    /**
     * Updates the existing account, email address can not be changed
     * @param name user's name
     * @param year user's school year
     * @param gender user's gender
     * @param major user's major
     * @return whether the account was updated.
     *         true if update is successful, false otherwise
     */
    public static boolean updateAccount(String name, Account.Year year, Character gender, String major) {
        long userId = currentUser.getId();

        Properties newUpdate = new Properties();
        newUpdate.setProperty(Account.KEY_NAME, name);
        newUpdate.setProperty(Account.KEY_GENDER, gender.toString());
        newUpdate.setProperty(Account.KEY_YEAR, year.getRepNumber() + "");
        newUpdate.setProperty(Account.KEY_MAJOR, major);

        currentUser.setName(name);
        currentUser.setGender(gender);
        currentUser.setSchoolYear(year);
        currentUser.setMajor(major);

        return DBManager.update(Account.class, userId, newUpdate);
    }


    /**
     * The function gets called when user decides to delete the account
     * @return true if delete success, false otherwise
     */
    public static boolean deleteAccount() {
        long userId = currentUser.getId();
        currentUser = null;
        return DBManager.deleteAccount(userId);
    }
}
