package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

/**
 * Created by Tadrill Perry on 5/21/2015.
 *
 * This class checks the DB updating with the calls from MPmanager
 */
public class MealPlanDBIntegrationTest extends TestCase {
    /**
     * Checks to see if the email exists in database
     * @param email the email of the user
     * @return the user id if email exists in the database
     *         -1 otherwise
     */
    private long emailExists(String email) {
        Properties whereKVs = new Properties();
        whereKVs.setProperty(Account.KEY_EMAIL, email);
        List<Properties> resList = DBManager.query(Account.class, whereKVs);

        // For every email address, there should be at most one account associated to it
        if (resList.isEmpty()) {
            return -1;
        } else {
            return Long.parseLong(resList.get(0).getProperty(Account.KEY_ID));
        }
    }

    String tad = "Tadrill Perry";
    String thao = "Thao Nguyen Dang";
    String ian = "Ian Junhao Zhu";
    long tadID = emailExists("tadrip@uw.edu");
    long ianID = emailExists("jhzhu10@uw.edu");
    long thaoID = emailExists("thaond@uw.edu");
    Calendar c = Calendar.getInstance();
    String food = "spam";
    String location = "hizzle";
    String descrip = "Grub a dub dub";
    Long mealID;
    Properties kv;
    List<Properties> pList;
    protected void setUp() {
        c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
        c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
        mealID = MealPlanManager.addPlan(tad, tadID, c, food, location, descrip);
    }

    protected void tearDown() {
        MealPlanManager.leavePlan(tadID, mealID);
        MealPlanManager.leavePlan(ianID, mealID);
        MealPlanManager.leavePlan(thaoID, mealID);
    }

    protected void test() {
        assertTrue(mealID != -1);
        // add some people to the plan!
        boolean attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("couldn't add someone to the meal plan", attempt);

        kv = new Properties();
        kv.setProperty(MealPlan.KEY_ID, Long.toString(mealID));
        pList = DBManager.query(MealPlan.class, kv);
        assertNotNull(pList);
        // Check that the mealPlan added has the proper properties in the DB
        assertEquals(pList.get(0).getProperty(MealPlan.KEY_FOODMOOD), food);
        assertEquals(pList.get(0).getProperty(MealPlan.KEY_DESCRIPTION), descrip);
        assertEquals(pList.get(0).getProperty(MealPlan.KEY_HOSTNAME), tad);
        assertEquals(pList.get(0).getProperty(MealPlan.KEY_LOCATION), location);
        assertEquals(pList.get(0).getProperty(MealPlan.KEY_HOST_ID), Long.toString(tadID));

        attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("couldn't add someone to the meal plan", attempt);
        attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("couldn't add someone to the meal plan", attempt);

        kv = new Properties();
        kv.setProperty(Attending.KEY_MEAL_PLAN_ID, Long.toString(mealID));
        pList = DBManager.query(Attending.class, kv);
        assertNotNull(pList);
        assertTrue(pList.size() >= 2); // not sure if the host is in the attending DB or not

        List<String> names = new ArrayList<>();
        assertTrue(1 == MealPlanManager.getAttendees(tadID, mealID, names));
        assertTrue(MealPlanManager.getHostingList(tadID).size() > 0); // The plan I made
        assertTrue(MealPlanManager.query(null, null, null, null, food).size() > 0);
    }
}
