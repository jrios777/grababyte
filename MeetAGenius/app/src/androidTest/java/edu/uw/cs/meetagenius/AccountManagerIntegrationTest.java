package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

import java.util.List;
import java.util.Properties;

/**
 * Created by STF_Student on 5/15/2015.
 *
 * Small Integration test for the AccountManager
 */
public class AccountManagerIntegrationTest extends TestCase {
    /**
     * Checks to see if the email exists in database
     * @param email the email of the user
     * @return the user id if email exists in the database
     *         -1 otherwise
     */
    private long emailExists(String email) {
        Properties whereKVs = new Properties();
        whereKVs.setProperty(Account.KEY_EMAIL, email);
        List<Properties> resList = DBManager.query(Account.class, whereKVs);

        // For every email address, there should be at most one account associated to it
        if (resList.isEmpty()) {
            return -1;
        } else {
            return Long.parseLong(resList.get(0).getProperty(Account.KEY_ID));
        }
    }
    // In a future integration test we will have to test that
    // the email is updated in the database correctly.
    // and calling / testing AccountManager.getUserId()
    //
    // This goes through a somewhat normal sequence of events
    String email = "email@testaccountIntegration.com";
    String password = "1234567";
    public void testSignUpAndUpdate() {
        String name = "test1";
        char gender = 'F';
        String major = "CSE";
        // email shouldn't exist yet
        assertTrue(emailExists(email) == -1);
        // user should not exist
        assertFalse(AccountManager.login(email));
        // log them out before logging on
        AccountManager.logout();
        // email exists in database now
        assertTrue(emailExists(email) != -1);
        // log in finally
        assertTrue(AccountManager.login(email));
        // Make sure userid matches accross these 2 methods
        assertEquals(emailExists(email), AccountManager.getUserId());
        // Log the user out
        AccountManager.logout();
        // Log them back in
        assertTrue(AccountManager.login(email));
        // update the account with vars from the top of this test
        assertTrue(AccountManager.updateAccount(name, (Account.Year.UNDERGRAD), 'F', "CSE"));
        // Check the getters and make sure they return the information that was just updated////////
        assertEquals("The email given does not match email signed up with",
                email, AccountManager.getUserEmail());
        assertEquals(gender, AccountManager.getUserGender());
        assertEquals(major, AccountManager.getUserMajor());
        assertEquals(name, AccountManager.getUserName());
        assertEquals(Account.Year.UNDERGRAD, AccountManager.getUserSchoolYear());
        ///////////////////////// End of information validation ////////////////////////////////////
        // Log them out again
        AccountManager.logout();
        // Log them back in
        assertTrue(AccountManager.login(email));
        // delete account
        assertTrue(AccountManager.deleteAccount());
        // Make sure they can't login with deleted account
        assertFalse(AccountManager.login(email));
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        if (AccountManager.login(email))
            AccountManager.deleteAccount();
    }
}
