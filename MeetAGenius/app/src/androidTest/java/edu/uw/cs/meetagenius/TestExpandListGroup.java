package edu.uw.cs.meetagenius;

import junit.framework.TestCase;
import java.util.Calendar;

/**
 * Created by Owner on 5/15/2015.
 *
 * Unit tests for the ExpandableListGroup class
 */
public class TestExpandListGroup extends TestCase {

    ExpandListGroup test;
    /**
     *  Creates  a new ExpandListGroup with the default constructor
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        test = new ExpandListGroup();
    }

    /**
     * Tests the default value of getHost()
     */
    public void testGetHost() {
        assertEquals(test.getHost(), "");
    }

    /**
     * Tests the default value of getDate()
     */
    public void testGetDate() {
        assertEquals(test.getDate(), "");
    }

    /**
     * Tests the default value of getFood()
     */
    public void testGetFood() {
        assertEquals(test.getFood(), "");
    }

    /**
     * Tests the default value of getTime()
     */
    public void testGetTime() {
        assertEquals(test.getTime(), "");
    }


    /**
     * Tests that setHost() changes the host name.
     */
    public void testSetHost() {
        test.setHost("Hal");
        assertEquals(test.getHost(), "Hal");
    }

    /**
     * Tests that setDate() changes the date.
     */
    public void testSetDate() {
        String newDate = "1/2/2015";
        test.setDate(newDate);
        assertEquals(test.getDate(), newDate);
    }

    /**
     *Tests that setFood() changes the food.
     */
    public void testSetFood() {
        String newFood = "wontons";
        test.setFood(newFood);
        assertEquals(test.getFood(), newFood);
    }

    /**
     * Tests that setTime() changes the time.
     */
    public void testSetTime() {
        String newTime = "1:00 p";
        test.setTime(newTime);
        assertEquals(test.getTime(), newTime);
    }

    /**
     * Tests that setDateAndTime() changes the date and time.
     */
    public void testSetDateAndTime() {
        Calendar rightNow = Calendar.getInstance();
        test.setDateAndTime(rightNow);
        // String format is not specified...
        // assertEquals(test.getTime(), rightNow.);
    }

}
