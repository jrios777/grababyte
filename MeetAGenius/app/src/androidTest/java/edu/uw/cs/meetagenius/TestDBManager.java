package edu.uw.cs.meetagenius;

import android.content.ContentValues;


import junit.framework.TestCase;

import java.util.List;
import java.util.Properties;

/**
 * Created by Tad Perry on 5/10/2015.
 *     /**
 * queries the given table to for all values where the given keys match the given values
 * table the table to query from
 * whereKeys the keys of the table to filter by, null will return everything.
 * whereVals the values of the keys of the table to filter with.
 * whereTypes the types of the keys associated with
 *  a list of ContentValues containing the results of the query, or null on error.
 * public static List<ContentValues> query(Table table, String[] whereKeys, String[] whereVals, KeyType[] whereTypes) {
 */

public class TestDBManager extends TestCase {
    private List<Properties> pList;

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////Test Insert/Delete///////////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    // The problem here is that this new account is gonna hang out in the database.
    // No remove for Accounts. Josh you can comment this out if you're reading it or wondering why
    // You have some "test" people in your database. But this tests out insert and then queries
    // the new insertion
    public void testInsertAndDelete() {
        Properties kv = new Properties();
        kv.setProperty(Account.KEY_NAME, "test");
        kv.setProperty(Account.KEY_GENDER, "M");
        long id = DBManager.insert(Account.class, kv);
        assertTrue(id != 0 && id != -1);
        kv = new Properties();
        kv.setProperty(Account.KEY_ID, Long.toString(id));
        kv.setProperty(Account.KEY_EMAIL, "fake@test.com");
        // Insert new Properties with no Gender but same id
        assertEquals(0, DBManager.insert(Account.class, kv));
        // Query on new addition
        pList = DBManager.query(Account.class, kv);
        assertNotNull(pList);
        assertEquals("M", pList.get(0).getProperty(Account.KEY_GENDER));
    }

    public void testUpdateAndLeavePlan() {
        Properties kv = new Properties();
        kv.setProperty(Attending.KEY_MEAL_PLAN_ID, Long.toString(1));
        kv.setProperty(Attending.KEY_ATTENDEE_ID, Long.toString(9));
        Long res = DBManager.insert(Attending.class, kv);
        assertTrue("Didn't get an id when adding to attendees", res > 0);
        assertTrue(DBManager.leavePlan(1, 9));
    }

    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////SIMPLE ACCOUNT QUERIES///////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void testAccountTableFirstPerson() {
        List<Properties> properties = grabPerson(1);
        assertNotNull(properties);
        Properties person = properties.get(0);
        assertEquals("Dan Radion", person.getProperty(Account.KEY_NAME));
        assertEquals("dradion@example.com", person.getProperty(Account.KEY_EMAIL));
        assertEquals("M", person.getProperty(Account.KEY_GENDER));
        assertEquals("3", person.getProperty(Account.KEY_MAJOR));
    }

    public void testAccountSecondPerson() {
        List<Properties> properties = grabPerson(2);
        assertNotNull(properties);
        Properties person = properties.get(0);
        assertEquals("Siena Ang", person.getProperty(Account.KEY_NAME));
        assertEquals("sienaang@example.com", person.getProperty(Account.KEY_EMAIL));
        assertEquals("F", person.getProperty(Account.KEY_GENDER));
        assertEquals("4", person.getProperty(Account.KEY_MAJOR));
    }


    public void testAccountMiddlePerson() {
        List<Properties> properties = grabPerson(5);
        assertNotNull(properties);
        Properties person = properties.get(0);
        assertEquals("Kaleo Brandt", person.getProperty(Account.KEY_NAME));
        assertEquals("kkbrandt@uw.edu", person.getProperty(Account.KEY_EMAIL));
        assertEquals("M", person.getProperty(Account.KEY_GENDER));
    }

    public void testAccountBottomPerson() {
        List<Properties> properties = grabPerson(10);
        assertNotNull(properties);
        Properties person = properties.get(0);
        assertEquals("Alice Cheng", person.getProperty(Account.KEY_NAME));
        assertEquals("xalicex1@uw.edu", person.getProperty(Account.KEY_EMAIL));
        assertEquals("F", person.getProperty(Account.KEY_GENDER));
        assertEquals("3", person.getProperty(Account.KEY_MAJOR));
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////
    ///////////////////////////////////SIMPLE MEAL PLAN QUERIES/////////////////////////////////////
    ////////////////////////////////////////////////////////////////////////////////////////////////

    public void testMealPlanTopRow() {
        List<Properties> properties = grabMeal(1);
        assertNotNull(properties);
        Properties meal = properties.get(0);
        assertEquals(1, Long.parseLong(meal.getProperty(MealPlan.KEY_HOST_ID)));
        assertEquals("2015-05-20 19:00:00", meal.getProperty(MealPlan.KEY_TIME));
        assertEquals("Borshch", meal.getProperty(MealPlan.KEY_FOODMOOD));
        assertEquals("CSE Atrium", meal.getProperty(MealPlan.KEY_LOCATION));
        assertEquals("Who wants to eat traditional Ukranian food?",
                meal.getProperty(MealPlan.KEY_DESCRIPTION));
    }

    public void testMealSecondRow() {
        List<Properties> properties = grabMeal(2);
        assertNotNull(properties);
        Properties meal = properties.get(0);
        assertEquals(4, Long.parseLong(meal.getProperty(MealPlan.KEY_HOST_ID)));
        assertEquals("2015-05-21 19:00:00", meal.getProperty(MealPlan.KEY_TIME));
        assertEquals("Vietnamese Pho", meal.getProperty(MealPlan.KEY_FOODMOOD));
        assertEquals("CSE Atrium", meal.getProperty(MealPlan.KEY_LOCATION));
        assertEquals("this is a description",
                meal.getProperty(MealPlan.KEY_DESCRIPTION));
    }

    public void testMealMiddleRow() {
        List<Properties> properties = grabMeal(5);
        assertNotNull(properties);
        Properties meal = properties.get(0);
        assertEquals(7, Long.parseLong(meal.getProperty(MealPlan.KEY_HOST_ID)));
        assertEquals("2015-05-24 18:30:00", meal.getProperty(MealPlan.KEY_TIME));
        assertEquals("Fried Chicken", meal.getProperty(MealPlan.KEY_FOODMOOD));
        assertEquals("CSE Atrium", meal.getProperty(MealPlan.KEY_LOCATION));
        assertEquals("this is a description",
                meal.getProperty(MealPlan.KEY_DESCRIPTION));
    }

    public void testMealBottomRow() {
        List<Properties> properties = grabMeal(9);
        assertNotNull(properties);
        Properties meal = properties.get(0);
        assertEquals(9, Long.parseLong(meal.getProperty(MealPlan.KEY_HOST_ID)));
        assertEquals("2015-05-26 16:30:00", meal.getProperty(MealPlan.KEY_TIME));
        assertEquals("Thai Tom", meal.getProperty(MealPlan.KEY_FOODMOOD));
        assertEquals("CSE Atrium", meal.getProperty(MealPlan.KEY_LOCATION));
        assertEquals("this is a description",
                meal.getProperty(MealPlan.KEY_DESCRIPTION));
    }


    public void testAttendingMealPlan1() {
        pList = grabAttendingMeal((long)1);
        assertTrue(3 <= pList.size());
        // Everyone's id should be less than 10
        assertTrue(Integer.parseInt(pList.get(0).getProperty(Attending.KEY_ATTENDEE_ID)) <= 10);
        assertTrue(Integer.parseInt(pList.get(1).getProperty(Attending.KEY_ATTENDEE_ID)) <= 10);
        assertTrue(Integer.parseInt(pList.get(2).getProperty(Attending.KEY_ATTENDEE_ID)) <= 10);
    }

    public void testAttendingMealPlan7() {
        pList = grabAttendingMeal((long)7);
        // Should be 1 people attending
        assertEquals(1, pList.size());
        assertTrue(Integer.parseInt(pList.get(0).getProperty(Attending.KEY_ATTENDEE_ID)) <= 10);
    }


    public void testAttendingMealPlan9() {
        pList = grabAttendingMeal((long)7);
        assertEquals(1, pList.size());
    }

    // Attending needs a constructor perhaps. Not neccessary I don't think
    private List<Properties> grabAttendingMeal(Long mealId) {
        Properties kv = new Properties();
        kv.setProperty(Attending.KEY_MEAL_PLAN_ID, Long.toString(mealId));
        return DBManager.query(Attending.class, kv);
    }

    public void testQueryingAccountsOnFemales() {
        Properties kv = new Properties();
        kv.setProperty(Account.KEY_GENDER, "F");
        pList = DBManager.query(Account.class, kv);
        assertNotNull(pList);
        assertTrue(5 >= pList.size());
        assertEquals(pList.get(0).getProperty(Account.KEY_GENDER), "F");
        assertEquals(pList.get(1).getProperty(Account.KEY_GENDER), "F");
        assertEquals(pList.get(pList.size() - 1).getProperty(Account.KEY_GENDER), "F");
    }

    /**
     * Grabs the row specified by id (The Account's id) and returns
     * the account's Content Values
     *
     * @param id the person's id
     * @return ContentValues for row specified
     */
    private List<Properties> grabPerson(long id) {
        Properties kv = new Properties();
        kv.setProperty(Account.KEY_ID, Long.toString(id));
        pList = DBManager.query(Account.class, kv);
        return pList;
    }

    /**
     * Grabs the row specified by id (The MealPlan's id) and returns
     * the MealPlan's Content Values
     *
     * @param id the MealPlan's id
     * @return ContentValues for row specified
     */
    private List<Properties> grabMeal(long id) {
        Properties kv = new Properties();
        kv.setProperty(MealPlan.KEY_ID, Long.toString(id));
        pList = DBManager.query(MealPlan.class, kv);
        return pList;
    }
}
