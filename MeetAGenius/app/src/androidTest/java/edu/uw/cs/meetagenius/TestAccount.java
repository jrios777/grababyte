package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

/**
 * Created by Tadrill Perry on 5/15/2015.
 *
 * Unit tests for the Account class
 */
public class TestAccount extends TestCase {
    String name = "Testman";
    String email = "tests@tests.com";
    Account.Year year = Account.Year.UNDERGRAD;
    int id = 100;
    String major = "street-livin";
    Account test;

    /**
     *  Creates  a new account using Account's 5-input constructor
     */
    @Override
    protected void setUp() throws Exception {
        super.setUp();
        test = new Account(name, email, year, 'M', id, major);
    }

    public void test5args() {
        // public Account(String name, String email, Year year, char gender, long id, String major)
        Account test5 = new Account(name, email, year, 'M', id, major);
        // not an exhaustive test of getters, because these are separate tests.
        assertEquals(name, test5.getName());
        assertEquals(email, test5.getEmail());
        assertEquals(id, test5.getId());
    }

    /**
     * Tests that the two-argument Account constructor works as desired.
     */
    public void test2args() {
        Account test2 = new Account(email, id);
        assertEquals(email, test2.getEmail());
        assertEquals(id, test2.getId());
        // The following tests are based on what the spec says. The spec will be updated.
        assertEquals(null, test2.getMajor());
        assertEquals(null, test2.getName());
        assertEquals(null, test2.getGender());
    }

    // These methods that are being tested no longer exist in the Account Class
//    /**
//     * Tests whether the test account is activated before and after calling activateAccount().
//     */
//    public void testActivateAccount() {
//        assertFalse(test.isActivated());
//        test.activateAccount();
//        assertTrue(test.isActivated());
//    }
//
//    /**
//     * Tests validation code get and set methods.
//     */
//    public void testValidationCode() {
//        assertEquals(-1, test.getValidationCode());
//        test.setValidationCode(1);
//        assertEquals(1, test.getValidationCode());
//        test.setValidationCode(2);
//        assertEquals(2, test.getValidationCode());
//    }

    /**
     * Tests that getSchoolYear() returns our original parameter
     */
    public void testGetYear() {
        assertEquals(Account.Year.UNDERGRAD, test.getSchoolYear());
    }

    /**
     * Tests that getName() returns our original parameter
     */
    public void testGetName() {
        assertEquals(name, test.getName());
    }

    /**
     * Tests that getEmail() returns our original parameter
     */
    public void testGetEmail() {
        assertEquals(email, test.getEmail());
    }

    /**
     * Tests that getId() returns our original parameter
     */
    public void testGetId() {
        assertEquals(id, test.getId());
    }

    /**
     * Tests that getGender() returns our original parameter
     */
    public void testGetGender() {
        assertEquals('M', test.getGender());
    }

    /**
     * Tests that getMajor() returns our original parameter
     */
    public void testGetMajor() {
        assertEquals(major, test.getMajor());
    }

    /**
     * Tests that setName() correctly changes the account's name
     */
    public void testSetName() {
        test.setName("changed");
        assertEquals("changed", test.getName());
    }

    /**
     * Tests that setYear() correctly changes the account's year
     */
    public void testSetYear() {
        Account.Year set = Account.Year.FACULTY;
        test.setYear(set);
        assertEquals(set, test.getSchoolYear());
        set = Account.Year.FRESHMAN;
        test.setYear(set);
        assertEquals(set, test.getSchoolYear());
    }

    /**
     * Tests that setGender() correctly changes the account's gender
     */
    public void testSetGender() {
        test.setGender('F');
        assertEquals('F', test.getGender());
    }

    /**
     * Tests that setMajor() correctly changes the account's major
     */
    public void testSetMajor() {
        String set = "switched it out";
        test.setMajor(set);
        assertEquals(set, test.getMajor());
    }

    /**
     * Tests that setEmail() correctly changes the account's email
     */
    public void testSetEmail() {
        String set = "changed@gmail";
        test.setEmail(set);
        assertEquals(set, test.getEmail());
    }

    ////////////////////////////// Account.Year Tests /////////////////////////////////////////////

    // No longer a getRepString method
//    public void testGetRepString() {
//        assertEquals("Undergrad", test.getSchoolYear().getRepString()); //(Account.Year.FACULTY));
//    }

    public void testGetRepNumber() {
        assertEquals(7, test.getSchoolYear().getRepNumber());
    }

    public void testToYear() {
        assertEquals(Account.Year.FRESHMAN, Account.Year.toYear(1));
    }

    public void testToYearUnknown() {
        assertEquals(Account.Year.UNKNOWN, Account.Year.toYear(0));
    }
}
