package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

import java.util.Calendar;
import java.util.List;

/**
 * Created by STF_Student on 5/15/2015.
 *
 * Unit tests for the MPManager
 */
public class TestMPManager extends TestCase {
    MealPlan plan;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        String hostName = "Tadrill Perry";
        long planID = 100;
        long hostID = 9;
        Calendar date = Calendar.getInstance();
        String foodMood = "waffles";
        String location = "the crib";
        String description = "I'm finna get sum grub";
        plan = new MealPlan(hostName, planID, hostID, date, foodMood, location, description);
    }

    public void testAddPlan() {
        assertTrue(0 < MealPlanManager.addPlan(plan.getHost(), plan.getPlanID(),
                plan.getDateAndTime(), plan.getFoodMood(), plan.getMeetingLocation(),
                plan.getDescription()));
    }

    public void testAttendPlan() {
        assertTrue(MealPlanManager.attendPlan(9, 1)); // Puts Tadrill into the meal plan 1
        // Leaves meal plan table as it was and tests leave plan
        assertTrue(MealPlanManager.leavePlan(9, 1));
    }

    public void testLeavePlan() {
        assertTrue(MealPlanManager.leavePlan(9, 1)); // Should not be in this mealplan
    }

    public void testQueryOnFakePerson() {
        List<MealPlan> mps = MealPlanManager.query(plan.getHost(), 'F', Account.Year.JUNIOR,
                plan.getDateAndTime(), plan.getFoodMood());
        assertTrue(mps.size() < 1); // There shouldn't be any female tadrill perry's in the database
    }

    public void testQeuryOnRealPerson() {
        List<MealPlan> mps = MealPlanManager.query(plan.getHost(),
                null, null, null, null);
        assertNotNull(mps);
        assertTrue(mps.size() > 0);
    }

    /**
     * Test that query with gender male works
     */
    public void testQueryOnMales() {
        List<MealPlan> mps = MealPlanManager.query(null, 'M', null, null, null);
        assertNotNull(mps);
        assertTrue(mps.size() > 0);
    }

    /**
     * Tests GetAllPlans has at least 9 plans.
     */
    public void testGetAllPlans() {
        List<MealPlan> mps = MealPlanManager.getAllPlans();
        assertNotNull(mps);
        assertTrue(mps.size() >= 9); // 9 meal plans in the database originally
    }

    /** Make sure when you get the hosting list, all the plans are hosted by the host with
     *  The id given to getHostingList
     */
    public void testGetHostingList() {
        List<MealPlan> mps = MealPlanManager.getHostingList(plan.getHostID());
        for (MealPlan m : mps) {
            assertEquals(m.getHost(), plan.getHost());
        }
    }

    /**
     * test getAttendee list for a person that is signed up for at least 1 mealplan
     */
    public void testGetAttendingList() {
        List<MealPlan> mps = MealPlanManager.getAttendingList(1);
        assertNotNull(mps);
    }
}
