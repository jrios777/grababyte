package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

/**
 * Created by Tadrill Perry on 5/21/2015.
 *
 * Runs a series of operations on the MealPlanManager :D D:
 * MealPlanManager
 */
public class MealPlanManagerIntegrationTest extends TestCase {
    /**
     * Checks to see if the email exists in database
     * @param email the email of the user
     * @return the user id if email exists in the database
     *         -1 otherwise
     */
    private long emailExists(String email) {
        Properties whereKVs = new Properties();
        whereKVs.setProperty(Account.KEY_EMAIL, email);
        List<Properties> resList = DBManager.query(Account.class, whereKVs);

        // For every email address, there should be at most one account associated to it
        if (resList.isEmpty()) {
            return -1;
        } else {
            return Long.parseLong(resList.get(0).getProperty(Account.KEY_ID));
        }
    }
    String tad = "Tadrill Perry";
    String tadMail = "tadrip@uw.edu";
    String thao = "Thao Nguyen Dang";
    String thaoMail = "thaond@uw.edu";
    String ian = "Ian Junhao Zhu";
    String ianMail = "jhzhu10@uw.edu";
    long tadID;
    long ianID;
    long thaoID;
    Calendar c = Calendar.getInstance();
    String food = "spam";
    String location = "hizzle";
    String descrip = "Grub a dub dub";
    Long mealID;
    protected void setUp() {
        boolean loggedIn = AccountManager.login(tadMail);
        assertTrue(loggedIn);
        tadID = AccountManager.getUserId();
        loggedIn = AccountManager.login(ianMail);
        assertTrue(loggedIn);
        ianID = AccountManager.getUserId();
        loggedIn = AccountManager.login(thaoMail);
        assertTrue(loggedIn);
        thaoID = AccountManager.getUserId();
        c.set(Calendar.HOUR_OF_DAY, c.getMinimum(Calendar.HOUR_OF_DAY));
        c.set(Calendar.MINUTE, c.getMinimum(Calendar.MINUTE));
        c.set(Calendar.SECOND, c.getMinimum(Calendar.SECOND));
        c.set(Calendar.MILLISECOND, c.getMinimum(Calendar.MILLISECOND));
        mealID = MealPlanManager.addPlan(tad, tadID, c, food, location, descrip);
    }

    protected void tearDown() {
        MealPlanManager.leavePlan(tadID, mealID);
        MealPlanManager.leavePlan(ianID, mealID);
        MealPlanManager.leavePlan(thaoID, mealID);
        AccountManager.logout();
    }

    /**
     * Goes through a series of method calls that might represent users joining/leaving
     * meal plans.
     */
    public void testMultipleMethods() {
        assertTrue(mealID != -1);
        // add some people to the plan!
        boolean attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("couldn't add someone to the meal plan", attempt);
        attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("couldn't add someone to the meal plan", attempt);
        attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("couldn't add someone to the meal plan", attempt);
        attempt = MealPlanManager.leavePlan(ianID, mealID);
        assertTrue("Couldn't leave plan", attempt);
        attempt = MealPlanManager.leavePlan(thaoID, mealID);
        assertTrue("Couldn't leave plan", attempt);
        attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue("Couldn't join plan", attempt);
        attempt = MealPlanManager.leavePlan(ianID, mealID);
        assertTrue(attempt);
        attempt = MealPlanManager.attendPlan(ianID, mealID);
        assertTrue(attempt);
        attempt = MealPlanManager.attendPlan(thaoID, mealID);
        assertTrue(attempt);
        // With this going on, try to get all the plans and make sure it isn't empty
        List<MealPlan> list = MealPlanManager.getAllPlans();
        assertNotNull(list);
        assertTrue(list.size() > 0);

        List<String> names = new ArrayList<>();
        assertTrue(1 == MealPlanManager.getAttendees(tadID, mealID, names));
        assertTrue("The three people are not in the attendee list: host and 2 people that joined",
                3 >= names.size()); // Not sure if the host is in this list, or if the host is in
                                    // the DB attendee table for this meal plan
        assertTrue(MealPlanManager.getHostingList(tadID).size() > 0); // The plan I made
        assertTrue(MealPlanManager.query(null, null, null, null, food).size() > 0);
    }
}
