package edu.uw.cs.meetagenius;

import android.test.AndroidTestCase;

import junit.framework.TestCase;

import java.util.Date;
import java.util.List;
import java.util.Properties;

/**
 * Created by Kaleo and Tadrill Perry on 4/21/2015
 *
 * Unit tests for the AccountManager class
 */
public class TestAccountManager extends TestCase {
    String email = "email@testaccount.com";
    String password = "1234567";
    String name = "test1";
    char gender = 'F';
    String major = "CSE";


    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        AccountManager.login(email);
        AccountManager.deleteAccount();
    }

    public void testLoginOnFakeAccount() {
        assertFalse(AccountManager.login("notevenreal@fake.com"));
    }

    // Account signed up in setUp()
    public void testLogin() {
        assertTrue(AccountManager.login(email));
    }

    public void testEmailExistsOnFakeEmail() {
        assertFalse(TestUtils.emailExists("hoohowbalognacows@gmail.com"));
    }

    public void testEmailExistsOnTestEmail() {
        assertTrue(TestUtils.emailExists(email));
    }

    public void testLogout() {
        AccountManager.logout();
        assertTrue(AccountManager.login(email));
        AccountManager.logout();
    }

    public void testUpdateAccountAndGetters() {
        assertTrue(AccountManager.updateAccount(name, (Account.Year.UNDERGRAD), 'F', "CSE"));
        assertEquals("The email given does not match email signed up with",
                email, AccountManager.getUserEmail());
        assertEquals(gender, AccountManager.getUserGender());
        assertEquals(major, AccountManager.getUserMajor());
        assertEquals(name, AccountManager.getUserName());
        assertEquals(Account.Year.UNDERGRAD, AccountManager.getUserSchoolYear());
    }

    // Not really sure how to test the email code, seeing as how it will have to email
    // a code to the user. Then I'm assuming the account's validation code is set
    // at the same time the email is sent.
}
