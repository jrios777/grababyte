package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

import java.util.Calendar;

/**
 * Created by STF_Student on 5/15/2015.
 *
 * Tests the MealPlan class
 */
public class TestMealPlan extends TestCase {
    //    public MealPlan(String hostName, long planID, long hostID, Calendar dateAndTime,
//    String foodMood, String location, String description) {
    String hostName = "Tadrill Perry";
    long planID = 100;
    long hostID = 9;
    Calendar date = Calendar.getInstance();
    String foodMood = "waffles";
    String location = "the crib";
    String description = "I'm finna get sum grub";
    MealPlan plan;

    @Override
    protected void setUp() {
        plan = new MealPlan(hostName, planID, hostID, date, foodMood, location, description);
    }

    public void testGetId() {
        assertEquals(planID, plan.getPlanID());
    }

    public void testGetHostid() {
        assertEquals(hostID, plan.getHostID());
    }

    public void testGetDateAndTime() {
        assertEquals(date, plan.getDateAndTime());
    }

    public void testHostName() {
        assertEquals(hostName, plan.getHost());
    }

    public void testGetFoodMood() {
        assertEquals(foodMood, plan.getFoodMood());
    }

    public void testGetDescription() {
        assertEquals(description, plan.getDescription());
    }

    public void testGetMeetingLocation() {
        assertEquals(location, plan.getMeetingLocation());
    }

    // I don't know how to write this test
    public void testDescribeContents() {

    }

}
