package edu.uw.cs.meetagenius;

import junit.framework.TestCase;

import java.util.List;
import java.util.Properties;

/**
 * Created by Tadrill Perry on 5/21/2015.
 *
 * This class extends JUnit's TestCase, and runs a series of operations
 * on the AccountManager and makes sure they are reflected in the Database.
 */
public class AccountManagerDBIntegrationTest extends TestCase {
    /**
     * Checks to see if the email exists in database
     * @param email the email of the user
     * @return the user id if email exists in the database
     *         -1 otherwise
     */
    private long emailExists(String email) {
        Properties whereKVs = new Properties();
        whereKVs.setProperty(Account.KEY_EMAIL, email);
        List<Properties> resList = DBManager.query(Account.class, whereKVs);

        // For every email address, there should be at most one account associated to it
        if (resList.isEmpty()) {
            return -1;
        } else {
            return Long.parseLong(resList.get(0).getProperty(Account.KEY_ID));
        }
    }
    private String email = "testemail@uw.edu";
    private String password = "123456";

    // Creates account in the database to ensure that when this test is ran
    // the account exists
    protected void setUp() {
        AccountManager.login(email);
        AccountManager.deleteAccount();
        AccountManager.logout();
    }
    // deletes the account from the database
    protected void tearDown() {
        AccountManager.login(email);
        AccountManager.deleteAccount();
    }

    public void testSignUpAndUpdate() {
        String name = "testIntegration";
        char gender = 'F';
        String major = "CSE";
        // email shouldn't exist yet
        assertTrue(emailExists(email) == -1);
        // check for email in the DBManager
        Properties kv = new Properties();
        kv.setProperty(Account.KEY_EMAIL, email);
        List<Properties> pList = DBManager.query(Account.class, kv);
        assertNotNull(pList);
        // Not sure exactly how Account.KEY_ACTIVATED is stored in the database.
//        assertEquals("An email that shouldn't exist, exists",
//                pList.get(0).getProperty(Account.KEY_ACTIVATED).equalsIgnoreCase("false"));
        // email exists in database now
        Long id = emailExists((email));
        assertTrue(id != -1);
        // Test account is in the database using DBManager
        kv = new Properties();
        kv.setProperty(Account.KEY_ID, Long.toString(id));
        pList = DBManager.query(Account.class, kv);
        assertNotNull(pList);
        assertEquals(pList.get(0).getProperty(Account.KEY_EMAIL), email);
        // log in finally
        assertTrue(AccountManager.login(email));
        // update the account with vars from the top of this test
        assertTrue(AccountManager.updateAccount(name, (Account.Year.UNDERGRAD), gender, major));
        // Check that the information was put into the DB with DBManager
        kv = new Properties();
        kv.setProperty(Account.KEY_ID, Long.toString(id));
        pList = DBManager.query(Account.class, kv);
        assertNotNull(pList);
        assertEquals(pList.get(0).getProperty(Account.KEY_EMAIL), email);
        assertEquals(pList.get(0).getProperty(Account.KEY_NAME), name);
        assertEquals(pList.get(0).getProperty(Account.KEY_GENDER), Character.toString(gender));
        assertEquals(pList.get(0).getProperty(Account.KEY_MAJOR), major);
        // Check the getters and make sure they return the information that was just updated////////
        assertEquals("The email given does not match email signed up with",
                email, AccountManager.getUserEmail());
        assertEquals(gender, AccountManager.getUserGender());
        assertEquals(major, AccountManager.getUserMajor());
        assertEquals(name, AccountManager.getUserName());
        assertEquals(Account.Year.UNDERGRAD, AccountManager.getUserSchoolYear());
        ///////////////////////// End of information validation ////////////////////////////////////
        // delete account
        assertTrue(AccountManager.deleteAccount());
        // Make sure the account is no longer activated
        kv = new Properties();
        kv.setProperty(Account.KEY_ID, Long.toString(id));
        pList = DBManager.query(Account.class, kv);
        assertNotNull(pList);
        // I am not sure if the account actually gets deactivated in the database
        // and if so how the string is stored.
        // assertTrue(activated.equalsIgnoreCase("false"));
    }
}
