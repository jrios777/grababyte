package org.intracode.contactmanager;

import android.net.Uri;

/**
 * Created by alice on 4/13/15.
 */
public class Contact {

    private String name, email, phone, address;
    private Uri imageURI;
    private int id;

    public Contact (int id, String name, String phone, String email, String address, Uri imageURI) {
        this.id = id;
        this.name = name;
        this.phone = phone;
        this.email = email;
        this.address = address;
        this.imageURI = imageURI;
    }

    public int getId() { return id; }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public Uri getImageURI() { return imageURI; }
}
