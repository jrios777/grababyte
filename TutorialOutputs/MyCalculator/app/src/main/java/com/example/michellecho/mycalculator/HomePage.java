package com.example.michellecho.mycalculator;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

/**
 * Created by MichelleCho on 4/17/15.
 */
public class HomePage extends Activity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.homepage);
        String username = getIntent().getStringExtra("Username");

        TextView tv = (TextView)findViewById(R.id.TVusername);
        tv.setText(username);
    }

    public void onButtonClick(View v) {
        EditText a1 = (EditText)findViewById(R.id.TFNum1);
        EditText a2 = (EditText)findViewById(R.id.TFNum2);

        TextView tv = (TextView)findViewById(R.id.Lresult);

        double num1 = 0;
        double num2 = 0;
        if (a1.getText().toString().length() != 0) {
            num1 = Double.parseDouble(a1.getText().toString());
        }
        if (a2.getText().toString().length() != 0) {
            num2 = Double.parseDouble(a2.getText().toString());
        }

        double ans = 0;
        boolean divByZero = false;

        if (v.getId() == R.id.Badd) {
            ans = num1 + num2;
        } else if (v.getId() == R.id.Bsub) {
            ans = num1 - num2;
        } else if (v.getId() == R.id.Bmult) {
            ans = num1 * num2;
        } else if (v.getId() == R.id.Bdiv) {
            if (num2 != 0) {
                ans = num1 / num2;
            } else {
                divByZero = true;
            }
        }
        if (divByZero) {
            tv.setText("Error. Cannot divide by 0.");
        } else {
            tv.setText(ans + "");
        }
    }

}
