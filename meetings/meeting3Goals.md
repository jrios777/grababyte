# Meeting 3

###### UPDATES 4/17
- changed the goals to better accomodate
- highlighted important reads to be done really soon... Should be REALLY useful for all our designing.
- note: Some readings will be removed/shortened as soon as I take a more careful look at what they cover. so if you're reading this just go ahead and read only the highlighted readings.

### Week Goals:
- __By Friday Meeting__: Finish Android Tutorial Playlists (Coded and everything, not just watched) ~~along with about a third of the following assigned readings~~
- __By Tuesday Meeting__: Team - Rough Draft of Architectural Design along with ~~following assigned readings~~ the highlighted ___(bold and italicized)___ sections 
  - Front End: UI Design Prototypes (layout files), Documented Stub Methods for each UI related class (such as activity classes)
  - Integration: Software Design Prototype, Documented Stub Methods for each non-UI related class
  - Testing: System Design Prototype, Learn Back End Interface, Documented Stub Methods for any Amazon SDK wrapper class.
- __By Next Friday Meeting__: Team - Final Draft of Architectural Design
- __By Next Tuesday Meeting__: Be ~70% done with the readings

### Assigned Readings
Most will come from the following guides:

- [Android Design Guide](https://developer.android.com/design/) - Easy read
- [Android Training Guide](https://developer.android.com/training/) - Difficult to digest, don't worry, just be sure to read carefully and mess around with code in subsections you think are relevant and useful to our project.
- [Android API Guide](https://developer.android.com/guide/) - Not too difficult to digest, mess around with code in subsections you think are relevant and useful to our project.
- [Android Tools Guide](https://developer.android.com/guide/) - (Mostly just for the testing team)

Sections assigned have multiple pages associated with it. Be sure to read it all, unless otherwise specified. Some sections will be read by multiple teams.

ALSO, don't feel overwhelmed! If something seems like you won't need it in the next three weeks, read it just once, but be ready to refer back to it.

### Front End
###### Android Design Guide

- ___All sections___ (this is an easy read, you don't have to watch the Videos though)

###### Android Training Guide 

- Getting Started
  - ___Building a Dynamic UI with Fragments___
  - Building Your First App
  - Supporting Different Devices
    - Supporting Different Screens
    - ~~Other 2 subsubsections~~
  - Adding the Action Bar
  - Managing the Activity Lifecycle
  - ~~Other 2 subsections~~
- Best Practices for Interaction and Engagement
  - ___Designing Effective Navigation___
  - ___Implementing Effective Navigation___
  - ~~Other 3 subsections~~
- Best Practices for User Interface
  - Designing for Multiple Screens
  - Creating Apps with Material Design
  - ~~Other 3 subsections~~
- Best Practices for User Input
  - Handling Keyboard Input
  - ~~Other 2 subsections~~
- Best Practices for Testing
  - Testing Your Android Activity
  - ~~Other subsection~~

###### Android API Guide

- Introduction
  - App Fundamentals
  - ~~Other 2 subsections~~
- App Components (Just the following subsections)
  - Intents and Intent Filters
  - Activities
- App Resources
  - Overview
  - Providing Resources
  - Accessing Resources
  - Handling Runtime Changes
  - ~~Other 2 subsections~~
- User Interface
  - Overview
  - Layouts
  - Input Control
  - Input Events
  - Action Bar
  - Styles and Themes
  - Anything else as needed as you design along
- Animation and Graphics (Optional)
- ~~Text and Input~~
- ~~Best Practices~~

###### Additional

- [Material Design](http://www.google.com/design/spec/material-design/introduction.html) (Optional) - Easy, quick read. Do remember that this would be for phones of API level 21 and over. If we support lower then we might need to separate different layouts for different API levels.

### Integration
###### Android Design Guide

- Patterns (not required, but recommended)
- ___Building Blocks___ (should be really short read)

###### Android Training Guide

- Getting Started
  - Building Your First App
  - Supporting Different Devices
    - Supporting Different Screens
    - ~~Other 2 subsubsections~~
  - Managing the Activity Lifecycle
  - Saving Data
    - Saving Key-Value Sets
    - Saving Files
    - ~~Other subsubsection~~
  - ~~Other 3 subsections~~
- Building Apps with Connectivity & the Cloud
  - Performing Network Operations
    - Connecting to the Network
    - Managing Network Usage
    - ~~Other subsubsection
  - ~~Other 6 subsections~~
- Best Practices for Interaction and Engagement
  - Designing Effective Navigation
  - Implementing Effective Navigation
  - Notifying the User
  - ~~Other 2 subsections~~
- Best Practices for User Interface
  - Designing for Multiple Screens
  - ___Creating Custom Views___
  - ~~Other 4 subsections~~
- ~~Best Practices for User Input~~
- Best Practices for Background Jobs
  - Running in a Background Service
  - Loading Data in the Background
- ___Best Practices for Performance___ (No need to read the last 2 subsections)
- Best Practices for Security & Privacy
  - ___Security Tips___
  - Security with HTTPS and SSL
  - ~~Other 2 subsections~~
- ~~Best Practices for Testing~~

###### Android API Guide

- Introduction
  - App Fundamentals
  - System Permissions
  - ~~Other subsection~~
- App Components
  - Activities
    - Loaders
    - ~~Other 3 subsubsections
  - Content Providers
  - Processes and Threads
  - ~~Other 3 subsections~~
- App Resources
  - Overview
  - Providing Resources
  - Accessing Resources
  - Handling Runtime Changes
  - ~~Other 2 subsections~~
- App Manifest (Minimum the following, recommended ~~2~~ 1 more) - a lot of these are pretty short
  - &lt;activity&gt;
  - &lt;application&gt;
  - &lt;category&gt;
  - &lt;intent-filter&gt;
  - &lt;manifest&gt;
  - ~~&lt;meta-data&gt;~~
  - ~~&lt;uses-sdk&gt;~~
  - &lt;uses-permission&gt;
- Text and Input (Optional)
- ~~Data Storage~~
- ~~Best Practices~~

### Back End
###### Android Training Guide

- Getting Started
  - ___Saving Data___
  - Building Your First App
  - ~~Other 5 subsections~~
- Building Apps with Connectivity & the Cloud
  - ___Performing Network Operations___
  - Transferring Data Using Sync Adapters
  - Transmitting Network Data Using Volley (Optional, we might use this if it seems helpful)
  - ~~Other 4 subsections~~
- Best Practices for Background Jobs
  - ___Loading Data in the Background___
  - ~~Other 2 subsections~~
- Best Practices for Performance
  - Managing Your App's Memory
  - Performance Tips
  - ~~Other 6 subsections~~
- Best Practices for Security & Privacy
  - Security with HTTPS and SSL
  - ~~Other 3 subsections~~
- ~~Best Practices for Testing~~

###### Android API Guide

- Introduction
- App Resources
- ___Data Storage___

###### Additional

- [~~Learn~~ Video about Amazon Cognito] (http://aws.amazon.com/cognito/)
- [Getting Started with Cognito] (http://docs.aws.amazon.com/cognito/devguide/getting-started/) - just that page
- [Cognito Concepts] (http://docs.aws.amazon.com/cognito/devguide/identity/concepts/) - just that section
- [Amazon Cognito Sync] (http://docs.aws.amazon.com/cognito/devguide/sync/) - just that section
- [AWS Mobile SDK Developer Guide] (http://docs.aws.amazon.com/mobile/sdkforandroid/developerguide/Welcome.html) - large reading, but we need to familiarize ourselves quickly, read the following sections:
  - What Is the AWS SDK for Android?
  - Getting Started with the AWS Mobile SDK for Android
    - Install AWS Mobile SDK for Android
    - Store and Retrieve Files with Amazon S3
    - Sync User Data with Cognito Sync
    - Store and Query App Data in DynamoDB
    - Building a Mobile Backend Using AWS Lambda
    - ~~Other 3 subsections~~
  - Set Up the SDK for Android
  - Amazon Cognito Identity
  - Amazon Simple Storage Service (S3)
  - Amazon DynamoDB
  - ~~Other 5 sections~~
- ~~[Using Cognito to Sync Data] (http://mobile.awsblog.com/post/Tx3GLBC11LK3IYV/Using-Amazon-Cognito-to-Sync-Data)~~
- ~~[Using Cognito Credentials Provider] (http://mobile.awsblog.com/post/TxR1UCU80YEJJZ/Using-the-Amazon-Cognito-Credentials-Provider)~~
- [What is Amazon SNS?] (http://docs.aws.amazon.com/sns/latest/dg/welcome.html) - just that page
- [Getting Started with Amazon SNS] (http://mobile.awsblog.com/post/TxR1UCU80YEJJZ/Using-the-Amazon-Cognito-Credentials-Provider) - just that section
- ~~[Managing Access] (http://docs.aws.amazon.com/sns/latest/dg/AccessPolicyLanguage.html)~~
- [Amazon SNS Mobile Push] (http://docs.aws.amazon.com/sns/latest/dg/SNSMobilePush.html) - just the following subsection
  - Getting Started with GCM
  - Using Amazon SNS Mobile Push
  - ~~Other 10 subsections~~

### Testing
###### Android Design Guide

- ___Patterns___ - to help get a sense of what Android UI Patterns are like and how you'd test that our app feels and behaves like a native Android app (for live person usage tests)

###### Android Training Guide

- Getting Started
  - Building Your First App
  - ~~Other 6 subsections~~
- ~~Best Practices for Performance~~
- Best Practices for Security & Privacy
  - Security Tips
  - Security with HTTPS and SSL
  - ~~Other 2 subsections~~
- ___Best Practices for Testing___

###### Android API Guide

- Introduction
  - App Fundamentals
  - ~~Other 2 subsections~~
- ~~App Resources~~
- ~~User Interface~~

###### Android Tools Guide

- Workflow (Just the following subsection)
  - ___Testing___ (Everything except the "From Eclipse" subsection)
- Testing Tools