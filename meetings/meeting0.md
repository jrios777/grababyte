# Meeting 0 
## April 3, 2015

### Couple of things figured out:
- Group Preferences Names Listing
- Group Preferences for Projects

### Skills / Experience
|Name       | Java      |Objective-C| XML       | SQL       | AWS       |Android Dev| iOS Dev   | C/C++     | Web Dev   |
|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|-----------|
|Alice      | Expert    | None      | ?         | ?         | ?         | None      | None      | ?         | ?         |
|Ian        | Expert    | None      | Familiar  | Familiar  | Limited   | None      | None      | Familiar  | None      |
|Josue      | Expert    | Limited   | Familiar  | Familiar  | Limited   | Proficient| Limited   | Proficient| Limited   |
|Jessica    | Expert    | None      | None      | None      | None      | None      | None      | Familiar  | Limited   |
|Kaleo      | Expert    | None      | None      | None      | None      | None      | None      | Familiar  | None      |
|Michelle   | Expert    | None      | Familiar  | Familiar  | Limited   | None      | None      | Familiar  | Familiar  |
|Tadrill    | Expert    | ?         | ?         | ?         | ?         | ?         | ?         | ?         | ?         |
|Thao       | Expert    | None      | Familiar  | Familiar  | Limited   | None      | None      | Familiar  | Limited   |