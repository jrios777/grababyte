# Meeting 1
## April 7, 2015

### Logistics
- Let's meet in the Research Commons for these meetings instead?
- Communication Tool: Ted doesn't have Facebook. Should we have him make one?
- A GitLab repo was created for our project. Take a look at the README file for instructions
- Within the next two weeks we need to quickly learn new tools and possibly new languages, please take a look at the resources posted at the bottom for learning

### Discussion Topics
- What are the requirements for our project?
  - Will it be multi-platform? Single platform?
    - Android Priority. iOS if there's time, Chrome if time
  - What features do we want to include?
    - 
  - What are the use cases?
- What roles are each us good at or are willing to do?
  - Project Management
  - Writing Technical Documents, such as weekly reports
  - Implementation
  - Validation / Quality Assurance
  - UI Design
  - Software Design
  - Learning new tools/languages
  - Front End Development
  - Back End Development
- How strong are the following for you?
  - Communication skills
  - Schedule load
  - Prioritization
  - Learning quickly
  - Motivation
  - Work Ethic
- What goals do you have for this project and for yourself?

### Ending Notes
- What is our next step as a collective group? How can we make sure we'll achieve this?
- What are our next steps as individuals? How can we make sure we achieve this?
- Some of us will be in the lab for the next hour or two, some of us could begin working on the requirements