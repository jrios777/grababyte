# Meeting X
## May 19, 2015

### Progress
- We have a Weekly Status Report due tonight. What are some successes? Besides 
the testing team finally having actually written a decent amount of unit tests
over this past week.

### Beta Release Demo
- Are we set to go with the demo? Any bug fixes that need to be done before the 
demo?

### Feature Complete Release Checklist
- Tests! Thoroughness is important
- Test coverage tool and description
  - Need execution record of this coverage tool
- Complete User Documentation
- Updated SRS
- Updated SDS
- Updated UML Diagrams
- Updated Developer Documentation
- Code style and performance optimization
- A file for changes since the Beta release

### MUST DO TODOS
- TESTING! Including, but not limited to,
  - Any additional unit tests not yet written
  - Any integration tests left to do
  - UI tests
  - Testing from the command line. (Jenkins needs to do this when it autobuilds
  so that we can be notified when someone breaks the build)
    - Unit tests
    - Integration tests
    - UI tests
  - Testing team, thoughts? What frameworks are you planning to use? Do you need
  extra assistance?
- UML Diagrams: Tad got a head start on this, but it's still incomplete, missing
  some elements and some fields and methods. Who can work on this?
- SRS, SDS, User Documentation updates: This should mostly just be login changes
  and any changes related to bug fixes, otherwise there probably isn't too much.
- Developer Documentation updates: Instructions need to be tried out. It should
  be flawless and clear.
- Website up to date and correct. I would like for the user guide to be a page
  rather than page of links.
  
### Additional TODOS
- Querying efficiency. As of now the MealPlanManager does several queries before
  returning all the plans for any query. This means that if there's 100 plans,
  it's talking to the web server 100 times. It's more efficient to dynamically
  load the attendees for each plan as plan information is shown than this.
  - Additionally, we should find a more effective way to load plans than loading
  all of them in case of large scaling.
- Transaction security. Those of you who took 344 know that if multiple SQL
  statements are going to be made they should be done in a transaction that can
  be rolled over in case something goes wrong. As of now that isn't the case and
  something as complicated as leaving the plan you're hosting may lead to
  inconsistency in the database if the web server somehow fails in the middle of
  execution.
- Push Notifications. Simply to notify users when another user joins/leaves a plan
  they're hosting/attending.

### Ending Notes
- How are we going to split this work up?