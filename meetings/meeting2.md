# Meeting 2
## April 10, 2015

### Logistics
- Is anybody willing to take notes at these meetings?
- How far is everyone in learning Android development?
  - Have you downloaded Android Studio?
  - Have you tried running an app?
  - Have you done the tutorials? Are they helpful?
- Some more resources:
  - [Genymotion](https://www.genymotion.com/#!/product). A much better, much faster, emulator for Android. Be sure to download the plugin too.
  - [Managing the Activity Lifecycle](http://developer.android.com/training/basics/activity-lifecycle/index.html). It should help you understand why and when we overwrite onCreate, onResume, onStart, etc.
  - [Best Practices for Testing](http://developer.android.com/training/testing.html)

### Discussion Topics
- Let's discuss the different parts of the requirements and provide feedback for each other.
  - Product Description
  - Use Cases
  - UI Diagrams
  - Process Description
- Using Git
  - Let's discuss branching and why we'd like to do that. Let's practice on a sample file.
  - Setting up Git for our Android application. (Android Studio has native support for Git)

### Ending Notes
- What is our next step as a collective group? How can we make sure we'll achieve this?
- What are our next steps as individuals? How can we make sure we achieve this?