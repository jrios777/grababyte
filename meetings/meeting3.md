# Meeting 3
## April 14, 2015

### Updates
- Did everyone finish the Android tutorials? How comfortable is everyone with Android development?
- Does everyone know what they're doing for this week? Is everyone comfortable with what they've been assigned or does anyone want to switch teams
- The Weekly Status Report is due tonight, it consists of three sections:
  - Goals from a week ago - in our case, mostly just learn Android Development
  - Progress we've made this week - what we've done, what worked, where we had trouble, and what we've learned
  - Plans and Goals for this following week - team goals and individual goals

### Git Branching/Merging Simulation (If you're reading this before the meeting, don't try this yet)
So since we're divided up into pretty modular teams I believe branching will be essential for us to work in parallel so let's try it out with this small example:
1. Open up the terminal and `cd` into the working repo directory
2. Type `git pull` so you can have the latest files and be up to date
3. Now, if you do `git status` it should say on the first line "On branch master". If not, let me know and I'll help you with that.
4. Now, type `git checkout -b FEATURE` where FEATURE is replaced with your netID (for now).
  - For example, I'd type `git checkout -b jrios777`. 
  - Let me explain what you just did and what `git branch` and `git checkout` do.
5. Once everyone has checked out a new branch, we're all going to create a new file called names.txt (Note the spelling) in our separate branches.
6. Inside of names.txt type your name and save the file.
7. Type `git add names.txt` to stage the changes.
8. Type `git commit -m "ANYTHING YOU WANT HERE"` to commit the changes to your repo
9. Type `git push origin FEATURE` where FEATURE is your netID.
  - What this does is it makes your branch public to everyone else in the project by pushing it to the head repo.
  - From here on, you'll only need to type `git push` to push to the head repo.
10. Type `git checkout master` to go back to the master branch. Notice your other branch is still intact if you just type `git checkout NET_ID`.
11. If you type `git status` it should say you're on the master branch and up to date
12. Try typing `git branch -d FEATURE` to see what happens. Let me explain what that means. Basically it's warning you that whatever you worked on in that branch is going to be lost if you don't merge.
13. Now, type `git diff master FEATURE` to see what's different between the master and your branch. It should just be that names.txt file
14. Now, here's the fun part, AT MY QUEUE, each of us are going to do all the following one person after another:
  1. Type `git merge FEATURE`.
  2. Type `git push` to push these changes to the head repo. It should fail.
  3. Type `git pull` to pull the latest files. You'll notice that the pull will output CONFLICT at the bottom and what file has the conflict.
  4. Open that file and resolve the conflicts. Keep the names in alphabetical order.
  5. Type `git status` to get the status of your repo. Notice it says "You have unmerged paths." and it also says "use "git add <file>..." to mark resolution".
  6. You just did the fix so type `git add names.txt` to stage the merge conflict resolvement.
  7. Type `git status` again. You'll notice it now says "All conflicts fixed but you are still merging. (use "git commit" to conclude merge)".
  8. Now commit the merge resolvement by typing `git commit -m "ANY MESSAGE YOU WANT"`
  9. And push it to the HEAD repo by typing `git push`.