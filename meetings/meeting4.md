# Meeting 4
## April 17, 2015

### Updates
- Everyone finished with the Android Tutorials?

### Discussion
- UI Design
  - Tabs? We only have 2 pages the users access frequently
- Software Architecture Design
  - What data abstractions should we use?
  - How will errors be handled?
- System Design
  - How will we ensure security?
- Testing Design
  - How are you going to test the other designs?
  - How will use cases be tested?

### Android Overview
- Runthrough of a simple app that gets the source files from my website
